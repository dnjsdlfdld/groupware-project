# 그룹웨어 API

***
### LANGUAGE
```
* JAVA 17
* SpringBoot
* JPA
```
***
### 기능
```
* 로그인
    - 관리자 로그인(post)
    - 직원 로그인(post)
    - 로그인 Response(get)  
```
```
* 공지사항
    1) 관리자용
        - 공지사항 등록(post)
        - 홈 화면(get)
```
```
* 배너
    1) 관리자용
        - 배너등록(post)
        - 홈화면용 배너(get)
        - 배너 리스트(get)
        - 게시일자 수정(put)
        - 배너 정보수정(put)
        - 배너삭제(delete)
```
```
* 직원관리
    1) 관리자용
        - 직원등록(post)
        - 직원 단수 조회(get)
        - 직원 상세정보(get)
        - 부서별 조회(get)
        - 이름별 조회(get)
        - 직급별 조회(get)
        - 직원 회사정보 수정(put)
        - 직원 개인정보 수정(put)
        - 직원으로 권한 변경(put)
        - 관리자로 권한 변경(put)
        - 직급 수정(put)
        - 퇴사처리(delete)
    
    2) 직원용
        - 내 정보 조회(get)
        - 비밀번호 변경(put)
        - 본인정보 수정(put)
```
```
* 출결관리
    - 작업중
```
```
* 휴무관리
    1) 관리자용
        - 연차 초기값 세팅(post)
        - 메인에 보여질 일부 정보 조회(get)
        - 휴가 상세정보 조회(get)
        - 휴가리스트 날짜 검색(get)
        - 연차 지급 및 차감(put)
        - 휴가 결재(put)
        
    2) 직원용
        - 휴가 신청(post)
        - 휴가 신청 정보보기(get)
        - 년도별 휴가 사용정보 조회(get)
```
***

### - SWAGGER
> * 로그인 SWAGGER
![SwaggerList](./images/swagger1.png)

> * 직원관리 SWAGGER
![SwaggerList](./images/swagger2.png)

> * 출결관리 SWAGGER
![SwaggerList](./images/swagger3.png)

> * 휴가관리 SWAGGER
![SwaggerList](./images/swagger4.png)

> * 공지사항 SWAGGER
![SwaggerList](./images/swagger5.png)

> * 배너관리 SWAGGER
![SwaggerList](./images/swagger6.png)
***


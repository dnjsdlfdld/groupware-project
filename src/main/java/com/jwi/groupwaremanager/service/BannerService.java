package com.jwi.groupwaremanager.service;

import com.jwi.groupwaremanager.entity.Banner;
import com.jwi.groupwaremanager.exception.CMissingDataException;
import com.jwi.groupwaremanager.model.banner.*;
import com.jwi.groupwaremanager.model.common.ListResult;
import com.jwi.groupwaremanager.repository.BannerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class BannerService {
    private final BannerRepository bannerRepository;

    /**
     * 배너 등록
     *
     * @param request
     */
    public void setBanner(BannerRequest request) {
        Banner banner = new Banner.BannerBuilder(request).build();
        bannerRepository.save(banner);
    }

    /**
     * 홈 화면용 배너정보 보여주기
     *
     * @return
     */
    public ListResult<BannerHomeItem> getBannerHome() {
        List<Banner> banners = bannerRepository.findTop5ByOrderByIdDesc();
        List<BannerHomeItem> result = new LinkedList<>();
        banners.forEach(banner -> {
            BannerHomeItem bannerHomeItem = new BannerHomeItem.BannerItemBuilder(banner).build();
            result.add(bannerHomeItem);
        });

        return ListConvertService.settingResult(result);
    }

    /**
     * 배너 상세정보 조회
     *
     * @return
     */
    public ListResult<BannerDetailItem> getBannerDetail() {
        List<Banner> banners = bannerRepository.findAll();
        List<BannerDetailItem> result = new LinkedList<>();
        banners.forEach(banner -> {
            BannerDetailItem bannerDetailItem = new BannerDetailItem.BannerDetailItemBuilder(banner).build();
            result.add(bannerDetailItem);
        });
        return ListConvertService.settingResult(result);
    }

    /**
     * 배너 정보수정 (제목, 이미지 URL)
     *
     * @param bannerId
     * @param request
     */
    public void putBanner(long bannerId, PutBannerRequest request) {
        Banner banner = bannerRepository.findById(bannerId).orElseThrow(CMissingDataException::new);
        banner.putBanner(request);
        bannerRepository.save(banner);
    }

    /**
     * 배너 게시기간 수정
     *
     * @param bannerId
     * @param request
     */
    public void putBannerDate(long bannerId, PutBannerDateRequest request) {
        Banner banner = bannerRepository.findById(bannerId).orElseThrow(CMissingDataException::new);
        banner.putBannerDate(request);
        bannerRepository.save(banner);
    }

    /**
     * 배너 삭제
     *
     * @param bannerId
     */
    public void delBanner(long bannerId) {
        bannerRepository.deleteById(bannerId);
    }

}

package com.jwi.groupwaremanager.service;

import com.jwi.groupwaremanager.entity.Member;
import com.jwi.groupwaremanager.enums.member.Position;
import com.jwi.groupwaremanager.exception.*;
import com.jwi.groupwaremanager.model.common.ListResult;
import com.jwi.groupwaremanager.model.member.*;
import com.jwi.groupwaremanager.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MemberService {
    private final MemberRepository memberRepository;

    /**
     * 사용자가 기입한 username 의 값 중복 갯수 확인
     *
     * @param username
     * @return boolean
     */
    private boolean isCount(String username) {
        long dupCount = memberRepository.countByUsername(username);

        return dupCount >= 1;
    }

    /**
     * 회원등록 메소드
     *
     * @param request
     * @return Member
     */
    public Member setMemberJoin(MemberJoinRequest request) {
        if (isCount(request.getUsername())) throw new COverlapUsernameException();

        Member member = new Member.MemberBuilder(request).build();
        return memberRepository.save(member);
    }

    /**
     * 사원 리스트에 보여질 정보 조회
     *
     * @return ListResult<MemberManageItem> 회원의 일부 데이터
     */
    public ListResult<MemberManageItem> getMembers() {
        List<Member> members = memberRepository.findAll();
        List<MemberManageItem> result = new LinkedList<>();

        members.forEach(member -> {
            MemberManageItem memberManageItem = new MemberManageItem.MemberManageItemBuilder(member).build();
            result.add(memberManageItem);

        });

        return ListConvertService.settingResult(result);
    }

    /**
     * 이름으로 회원의 상세정보 검색
     *
     * @param name
     * @return ListResult<MemberManagerItemDetail> 회원정보의 상세정보 반환
     */
    public ListResult<MemberManagerItemDetail> getMembersName(String name) {
        List<Member> members = memberRepository.findAllByNameAndIsEnableTrue(name);

        if (members.isEmpty()) throw new CMissingDataException();

        List<MemberManagerItemDetail> result = new LinkedList<>();
        members.forEach(member -> {
            MemberManagerItemDetail memberManagerItem = new MemberManagerItemDetail.MemberManagerItemBuilder(member).build();
            result.add(memberManagerItem);
        });
        return ListConvertService.settingResult(result);
    }

    /**
     * 회원 상세정보 조회(single)
     *
     * @param memberId
     * @return MemberManagerItemDetail 회원정보의 상세정보 반환
     */
    public MemberManagerItemDetail getMemberDetail(long memberId) {
        Member member = memberRepository.findById(memberId).orElseThrow(CNoInformationException::new);
        return new MemberManagerItemDetail.MemberManagerItemBuilder(member).build();
    }



    /**
     * 부서별 회원 상세정보 조회
     *
     * @param department
     * @return ListResult<MemberManagerItemDetail> 회원정보의 상세 정보 반환
     */
    public ListResult<MemberManagerItemDetail> getMemberDepartment(String department) {
        List<Member> members = memberRepository.findAllByDepartmentAndIsEnableTrueOrderByDateInDesc(department);
        List<MemberManagerItemDetail> result = new LinkedList<>();

        members.forEach(member -> {
            MemberManagerItemDetail memberManagerItem = new MemberManagerItemDetail.MemberManagerItemBuilder(member).build();
            result.add(memberManagerItem);
        });

        return ListConvertService.settingResult(result);
    }

    /**
     * 직급별 회원 상세정보 조회
     *
     * @param position > enum
     * @return ListResult<MemberManagerItemDetail> 회원정보의 상세 정보 반환
     */
    public ListResult<MemberManagerItemDetail> getMemberPosition(Position position) {
        List<Member> members = memberRepository.findAllByPositionAndIsEnableTrueOrderByDateInDesc(position);
        List<MemberManagerItemDetail> result = new LinkedList<>();
        members.forEach(member -> {
            MemberManagerItemDetail memberManagerItem = new MemberManagerItemDetail.MemberManagerItemBuilder(member).build();
            result.add(memberManagerItem);
        });

        return ListConvertService.settingResult(result);
    }

    /**
     * 직원의 내 정보 불러오기
     *
     * @param memberId
     * @return MemberUserItem 정보가 반환될 model
     */
    public MemberUserItem getMyInfo(long memberId) {
        Member member = memberRepository.findById(memberId).orElseThrow(CMissingDataException::new);
        return new MemberUserItem.MemberUserItemBuilder(member).build();
    }

    /**.
     * 직원 정보수정
     *
     * @param memberId
     * @param request
     */
    public void putMember(long memberId, PutMemberRequest request) {
        Member member = memberRepository.findById(memberId).orElseThrow(CMissingDataException::new);
        member.putMember(request);
        memberRepository.save(member);
    }

    /**
     * 비밀번호 수정
     *
     * @param memberId
     * @param request
     */
    public void putPassword(long memberId, PutMemberPasswordRequest request) {
        Member member = memberRepository.findById(memberId).orElseThrow(CMissingDataException::new);
        member.putPassword(request);
        memberRepository.save(member);
    }

    /**
     * 사용권한을 관리자로 수정
     *
     * @param memberId
     * @param request
     */
    public void putIsAdminManager(long memberId, MemberIsAdminRequest request) {
        Member member = memberRepository.findById(memberId).orElseThrow(CMissingDataException::new);
        member.putIsAdminManager(request);
        memberRepository.save(member);
    }


    /**
     * 사용권한을 직원으로 수정
     *
     * @param memberId
     */
    public void putIsAdmin(long memberId) {
        Member member = memberRepository.findById(memberId).orElseThrow(CMissingDataException::new);
        member.putIsAdmin();
        memberRepository.save(member);
    }

    /**
     * 직원 퇴사처리
     * 사용자에게는 delete로 보여지고 실제 put
     *
     * @param memberId
     */
    public void putIsEnable(long memberId) {
        Member member = memberRepository.findById(memberId).orElseThrow(CMissingDataException::new);

        if (!member.getIsEnable()) throw new CMissingDataException();

        member.putIsEnable();
        memberRepository.save(member);
    }

    /**
     * 직원의 내 정보 수정
     *
     * @param memberId
     * @param request
     */
    public void putUserInfo(long memberId, PutMemberUserRequest request) {
        Member member = memberRepository.findById(memberId).orElseThrow(CMissingDataException::new);
        member.putUserInfo(request);
        memberRepository.save(member);
    }

    /**
     * 다른 controller에서 회원 시퀀스가 필요한 경우
     *
     * @param memberId
     * @return
     */
    public Member getMemberId(long memberId) {
        return memberRepository.findById(memberId).orElseThrow(CValidNotAccountException::new);
    }

    /**
     * 직급 수정
     *
     * @param memberId
     * @param request
     */
    public void putMemberPosition (long memberId, PutMemberPositionRequest request) {
        Member member = memberRepository.findById(memberId).orElseThrow(CMissingDataException::new);

        if (!member.getIsEnable()) throw new CResignCompanyException();

        member.putPosition(request);
        memberRepository.save(member);
    }

    /**
     * 직원의 회사정보 수정
     *
     * @param memberId
     * @param request
     * 정보 수정에 필요한 request와 memberId
     */
    public void putMemberCompanyInfo(long memberId, PutMemberCompanyInfoRequest request) {
        Member member = memberRepository.findById(memberId).orElseThrow(CMissingDataException::new);

        if (!member.getIsEnable()) throw new CResignCompanyException();

        member.putMemberCompanyInfo(request);
        memberRepository.save(member);
    }
}
package com.jwi.groupwaremanager.service;

import com.jwi.groupwaremanager.entity.HolidayCount;
import com.jwi.groupwaremanager.entity.HolidayHistory;
import com.jwi.groupwaremanager.entity.HolidayUpload;
import com.jwi.groupwaremanager.entity.Member;
import com.jwi.groupwaremanager.enums.holiday.ApprovalState;
import com.jwi.groupwaremanager.exception.*;
import com.jwi.groupwaremanager.model.common.ListResult;
import com.jwi.groupwaremanager.model.holiday.HolidaySimpleList;
import com.jwi.groupwaremanager.model.holiday.HolidayUploadItem;
import com.jwi.groupwaremanager.model.holiday.HolidayUserRequest;
import com.jwi.groupwaremanager.repository.HolidayCountRepository;
import com.jwi.groupwaremanager.repository.HolidayHistoryRepository;
import com.jwi.groupwaremanager.repository.HolidayUploadRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class HolidayService {
    private final HolidayCountRepository holidayCountRepository;
    private final HolidayHistoryRepository holidayHistoryRepository;
    private final HolidayUploadRepository holidayUploadRepository;


    /**
     * 직원의 연차 초기값 등록
     *
     * @param memberId
     * @param dateIn
     */
    public void setCount(long memberId, LocalDate dateIn) {
        HolidayCount holidayCount = new HolidayCount.HolidayCountBuilder(memberId, dateIn).build();
        holidayCountRepository.save(holidayCount);
    }

    /**
     * 휴가 신청서 일부 정보 가져오기
     *
     * @return
     */
    public ListResult<HolidaySimpleList> getUploadSimpleList() {
        List<HolidayUpload> holidayUploads = holidayUploadRepository.findAll();
        List<HolidaySimpleList> result = new LinkedList<>();

        holidayUploads.forEach(holidayUpload -> {
            HolidaySimpleList holidaySimpleList = new HolidaySimpleList.HolidaySimpleListBuilder(holidayUpload).build();
            result.add(holidaySimpleList);
        });
        return ListConvertService.settingResult(result);
    }

    /**
     * memberId로 휴가 신청서 가져오기
     *
     * @param memberId
     * @return
     */
    public HolidayUploadItem getUserUpload(long memberId) {
        HolidayUpload holidayUpload = holidayUploadRepository.findById(memberId).orElseThrow(CMissingDataException::new);

        return new HolidayUploadItem.HolidayUploadItemBuilder(holidayUpload).build();
    }

    /**
     * uploadId로 휴가 신청 상세정보 가져오기
     *
     * @param uploadId
     * @return
     */
    public HolidayUploadItem getUserUploads(long uploadId) {
        HolidayUpload holidayUpload = holidayUploadRepository.findById(uploadId).orElseThrow(CMissingDataException::new);
        return new HolidayUploadItem.HolidayUploadItemBuilder(holidayUpload).build();
    }

    /**
     * 날짜별 휴가 정보 가져오기
     *
     * @param dateStart
     * @param dateEnd
     * @return
     */
    public ListResult<HolidayUploadItem> getDateUserUpload(LocalDate dateStart, LocalDate dateEnd) {
        LocalDateTime dateStartTime = LocalDateTime.of(
                dateStart.getYear(),
                dateStart.getMonthValue(),
                dateStart.getDayOfMonth(),
                0, 0, 0
        );

        LocalDateTime dateEndTime = LocalDateTime.of(
                dateEnd.getYear(),
                dateEnd.getMonthValue(),
                dateEnd.getDayOfMonth(),
                23, 59, 59
        );
        List<HolidayUpload> holidayUploads = holidayUploadRepository.findAllByStartHolidayGreaterThanEqualAndStartHolidayLessThanEqualOrderByIdDesc
                (dateStartTime, dateEndTime);
        List<HolidayUploadItem> result = new LinkedList<>();
        holidayUploads.forEach(holidayUpload -> {
            HolidayUploadItem holidayUploadItem = new HolidayUploadItem.HolidayUploadItemBuilder(holidayUpload).build();
            result.add(holidayUploadItem);
        });
        return ListConvertService.settingResult(result);
    }

    /**
     * 휴가 승인 및 반려 처리
     *
     * @param holidayUpload
     * @param isEnable
     */
    public void putHolidayState(HolidayUpload holidayUpload, boolean isEnable) {
        HolidayUpload holidayUploadId = holidayUploadRepository.findById(holidayUpload.getId()).orElseThrow(CMissingDataException::new);

        if (holidayUpload.getApprovalState().equals(ApprovalState.COMPLETED_APPROVAL)) throw new CNoSameDataException();
        if (holidayUpload.getApprovalState().equals(ApprovalState.RETURN_APPROVAL)) throw new CDoNotPasswordException();

        if (isEnable) holidayUpload.putApprovalState();
        else holidayUpload.putCompanion();
        holidayUploadRepository.save(holidayUploadId);

        HolidayCount holidayCount = holidayCountRepository.findById(holidayUpload.getMember().getId()).orElseThrow(CMissingDataException::new);
        if (isEnable) holidayCount.plusCountUse(holidayUpload.getUseHoliday());
        holidayCountRepository.save(holidayCount);
    }

    /**
     * 휴가 반려처리
     *
     * @param uploadId
     * @param member
     */
    public void putCompanion(long uploadId, Member member) {
        Optional<HolidayUpload> findHolidayUpload = holidayUploadRepository.findByIdAndMember_Id(uploadId, member.getId());

        if (findHolidayUpload.get().getApprovalState().equals(ApprovalState.RETURN_APPROVAL)) throw new CAlreadyReturnApprovalException();

        HolidayUpload holidayUpload = findHolidayUpload.get();
        holidayUpload.putCompanion();
        holidayUploadRepository.save(holidayUpload);
    }

    /**
     * 휴가 승인처리
     *
     * @param member
     * @param holidayUpload
     */
    public void putUserHolidayCompleted(Member member, HolidayUpload holidayUpload) {
        HolidayCount holidayCount = holidayCountRepository.findById(member.getId()).orElseThrow(CMissingDataException::new);
        HolidayUpload holidayUploadId = holidayUploadRepository.findById(holidayUpload.getId()).orElseThrow(CDoNotPasswordException::new);

        // 승인 완료가 되면 해당 일수만큼 차감되게

        holidayCount.plusCountUse(holidayUpload.getUseHoliday());
        holidayUpload.putApprovalState();
        holidayUploadRepository.save(holidayUploadId);
        holidayCountRepository.save(holidayCount);


        HolidayHistory holidayHistory = new HolidayHistory.HolidayHistoryUploadBuilder(member, holidayUpload).build();
        holidayHistoryRepository.save(holidayHistory);
    }

    /**
     * 연차 지급 및 차감하기
     *
     * @param member
     * @param isGive
     * @param increaseOrDecreaseValue
     */
    public void putHolidayCount(Member member, boolean isGive, float increaseOrDecreaseValue) {
        HolidayCount holidayCount = holidayCountRepository.findById(member.getId()).orElseThrow(CValidNotAccountException::new);

        if (isGive) holidayCount.plusCountUse(increaseOrDecreaseValue);
        else holidayCount.plusCountTotal(increaseOrDecreaseValue);
        holidayCountRepository.save(holidayCount);

        HolidayHistory holidayHistory = new HolidayHistory.HolidayHistoryBuilder(member, isGive, increaseOrDecreaseValue).build();
        holidayHistoryRepository.save(holidayHistory);
    }

    /**
     * 휴가 신청하기
     * 신청일수가 보유일수 보다 많으면 에러처리
     *
     * @param member
     * @param request
     */
    public void setHolidayUser(Member member, HolidayUserRequest request) {
        HolidayCount holidayCount = holidayCountRepository.findById(member.getId()).orElseThrow(CMissingDataException::new);

        if (holidayCount.getHaveHoliday() < request.getUseHoliday()) throw new CCheckHolidayException();

        HolidayUpload holidayUpload = new HolidayUpload.HolidayUploadBuilder(member, request).build();
        holidayUploadRepository.save(holidayUpload);
    }

    /**
     * 휴가 신청서 년도별 조회
     *
     * @param memberId
     * @param selectYear
     * @return
     */
    public ListResult<HolidayUploadItem> getYearUploads(long memberId, int selectYear) {
        LocalDateTime dateStart = LocalDateTime.of(selectYear, 1, 1, 0, 0, 0);
        LocalDateTime dateEnd = LocalDateTime.of(selectYear, 12, 31, 23, 59, 59);
        List<HolidayUpload> holidayUploads = holidayUploadRepository.findAllByDateCreateGreaterThanEqualAndDateCreateLessThanEqualAndMember_Id
                (dateStart, dateEnd, memberId);
        List<HolidayUploadItem> result = new LinkedList<>();
        holidayUploads.forEach(holidayUpload -> {
            HolidayUploadItem holidayUploadItem = new HolidayUploadItem.HolidayUploadItemBuilder(holidayUpload).build();
            result.add(holidayUploadItem);
        });
        return ListConvertService.settingResult(result);
    }


    /**
     * uploadId의 entity 정보 반환
     *
     * @param uploadId
     * @return
     */
    public HolidayUpload getUploadId(long uploadId) {
        return holidayUploadRepository.findById(uploadId).orElseThrow(CMissingDataException::new);
    }
}

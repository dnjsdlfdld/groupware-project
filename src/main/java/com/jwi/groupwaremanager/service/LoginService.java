package com.jwi.groupwaremanager.service;

import com.jwi.groupwaremanager.entity.Member;
import com.jwi.groupwaremanager.exception.CDoNotPasswordException;
import com.jwi.groupwaremanager.exception.CDoNotPinPasswordException;
import com.jwi.groupwaremanager.exception.CResignCompanyException;
import com.jwi.groupwaremanager.exception.CValidNotAccountException;
import com.jwi.groupwaremanager.model.login.ManagerLoginRequest;
import com.jwi.groupwaremanager.model.login.MemberLoginRequest;
import com.jwi.groupwaremanager.model.login.LoginResponse;
import com.jwi.groupwaremanager.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class LoginService {
    private final MemberRepository memberRepository;

    /**
     * 관리자용 로그인
     *
     * @param request
     * @return LoginResponse 토큰에 저장될 데이터
     */
    public LoginResponse doLoginManager(ManagerLoginRequest request) {
        Member member = memberRepository.findByUsernameAndIsAdminTrue(request.getUsername()).orElseThrow(CValidNotAccountException::new);

        if (!member.getManagerPin().equals(request.getManagerPin())) throw new CDoNotPinPasswordException();
        if (!member.getPassword().equals(request.getPassword())) throw new CDoNotPasswordException();
        if (!member.getIsEnable()) throw new CResignCompanyException();

        Member loginTime = memberRepository.findByUsername(request.getUsername());
        loginTime.putLoginTime();
        memberRepository.save(loginTime);
        return new LoginResponse.MemberLoginResponseBuilder(member).build();
    }

    /**
     * 직원 로그인
     *
     * @param request
     * @return LoginResponse 토큰에 저장될 데이터
     */
    public LoginResponse doLoginUser(MemberLoginRequest request) {
        Member member = memberRepository.findByUsernameAndIsAdminFalse(request.getUsername()).orElseThrow(CValidNotAccountException::new);

        if (!member.getPassword().equals(request.getPassword())) throw new CDoNotPasswordException();
        if (!member.getIsEnable()) throw new CResignCompanyException();

        Member loginTime = memberRepository.findByUsername(request.getUsername());
        loginTime.putLoginTime();
        memberRepository.save(loginTime);
        return new LoginResponse.MemberLoginResponseBuilder(member).build();
    }

    /**
     * 로그인 후 정보가 사용될 데이터
     *
     * @param memberId
     * @return
     */
    public LoginResponse getLoginInfo(long memberId) {
        Member member = memberRepository.findById(memberId).orElseThrow();

        return new LoginResponse.MemberLoginResponseInfoBuilder(member).build();
    }
}

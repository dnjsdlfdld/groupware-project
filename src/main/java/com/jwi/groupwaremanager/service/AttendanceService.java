package com.jwi.groupwaremanager.service;

import com.jwi.groupwaremanager.entity.Attendance;
import com.jwi.groupwaremanager.entity.Member;
import com.jwi.groupwaremanager.enums.attendance.AttendanceType;
import com.jwi.groupwaremanager.exception.*;
import com.jwi.groupwaremanager.model.attendance.AttendanceResponse;
import com.jwi.groupwaremanager.model.attendance.AttendanceDetailItem;
import com.jwi.groupwaremanager.model.attendance.AttendanceListItem;
import com.jwi.groupwaremanager.model.common.ListResult;
import com.jwi.groupwaremanager.repository.AttendanceRepository;
import com.jwi.groupwaremanager.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class AttendanceService {
    private final AttendanceRepository attendanceRepository;
    private final MemberRepository memberRepository;

    /**
     * 전 직원 출근정보 가져오기 (년, 월 주지 않음)
     *
     * @return
     */
    public ListResult<AttendanceListItem> getAttendanceList() {
        LocalDate dateStartTime = LocalDate.of(LocalDate.now().getYear(), LocalDate.now().getMonthValue(), LocalDate.now().getDayOfMonth());
        LocalDate dateEndTime = LocalDate.of(LocalDate.now().getYear(), LocalDate.now().getMonthValue(), LocalDate.now().getDayOfMonth());

        List<Attendance> attendances = attendanceRepository.findAllByDateWorkLessThanEqualAndDateWorkGreaterThanEqual(dateStartTime, dateEndTime);
        List<AttendanceListItem> result = new LinkedList<>();

        attendances.forEach(attendance -> {
            AttendanceListItem attendanceListItem = new AttendanceListItem.AttendanceListItemBuilder(attendance).build();
            result.add(attendanceListItem);
        });

        return ListConvertService.settingResult(result);
    }

    /**
     * 출근, 조퇴, 퇴근 처리 후 상태값 반환
     *
     * @param member
     * @param attendanceType
     * @return
     */
    public AttendanceResponse doAttendanceChange(Member member, AttendanceType attendanceType) {
        Optional<Attendance> attendance = attendanceRepository.findByDateWorkAndMemberId(LocalDate.now(), member.getId());

        Attendance attendanceResult;
        if (attendance.isEmpty()) attendanceResult = setAttendance(member);
        else attendanceResult = putAttendance(attendance.get(), attendanceType);

        return new AttendanceResponse.AttendanceResponseBuilder(attendanceResult).build();
    }

    /**
     * 현재 상태정보 가져오기
     *
     * @param memberId
     * @return
     */
    public AttendanceResponse getCurrentStatus(long memberId) {
        Optional<Attendance> attendance = attendanceRepository.findByDateWorkAndMemberId(LocalDate.now(), memberId);

        if (attendance.isEmpty()) return new AttendanceResponse.AttendanceResponseNoneBuilder().build();
        else return new AttendanceResponse.AttendanceResponseBuilder(attendance.get()).build();
    }

    // memberId가 출근을 누르면 Builder에 지정해놓은 설정들 값이 기입되고 그 값들이 data에 들어가고 반환된다.
    private Attendance setAttendance(Member member) {
        Attendance data = new Attendance.AttendanceTestBuilder(member).build();
        return attendanceRepository.save(data);
    }

    /**
     * 근태 처리 exception 및 근태상태 변경
     *
     * @param attendanceTest
     * @param attendanceType
     * @return
     */
    private Attendance putAttendance(Attendance attendanceTest, AttendanceType attendanceType) {
        // 출근 후에는 다시 출근 상태로 변경 불가.
        if (attendanceType.equals(AttendanceType.ATTENDANCE)) throw new CAlreadyAtWorkException();
        // Entity의 상태가 변하는 상태와 같을 때
        if (attendanceTest.getAttendanceType().equals(attendanceType)) throw new CNoSameDataException();
        // 퇴근 후에는 상태를 변경할 수 없다
        if (attendanceTest.getAttendanceType().equals(AttendanceType.LEAVE_WORK)) throw new CAlreadyLeaveWorkException();

        attendanceTest.putState(attendanceType);
        return attendanceRepository.save(attendanceTest);
    }

    /**
     * 근태 상세정보 가져오기
     *
     * @param memberId
     * @return
     */
    public AttendanceDetailItem getAttendanceDetail(long memberId) {
        Attendance attendance = attendanceRepository.findById(memberId).orElseThrow(CMissingDataException::new);
        return new AttendanceDetailItem.AttendanceDetailItemBuilder(attendance).build();
    }

    /**
     * 이달의 근무일수 가져오기
     *
     * @param member
     * @return
     */
    public long getMonthWorkCopy(Member member) {
        LocalDate dateStart = LocalDate.of(LocalDate.now().getYear(), LocalDate.now().getMonthValue(), 1);

        Calendar cal = Calendar.getInstance();

        int maxDay = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
        LocalDate dateEnd = LocalDate.of(LocalDate.now().getYear(), LocalDate.now().getMonthValue(), maxDay);

        return attendanceRepository.countByMember_IdAndDateWorkGreaterThanEqualAndDateWorkLessThanEqual
                (member.getId(), dateStart, dateEnd);
    }
}

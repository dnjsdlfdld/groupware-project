package com.jwi.groupwaremanager.service;

import com.jwi.groupwaremanager.entity.Notice;
import com.jwi.groupwaremanager.enums.notice.NoticeStatus;
import com.jwi.groupwaremanager.exception.CMissingDataException;
import com.jwi.groupwaremanager.model.common.ListResult;
import com.jwi.groupwaremanager.model.notice.*;
import com.jwi.groupwaremanager.repository.NoticeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;


@Service
@RequiredArgsConstructor
public class NoticeService {
    private final NoticeRepository noticeRepository;

    /**
     * 공지사항 등록
     *
     * @param noticeStatus 공개여부
     * @param request
     */
    public void setNotice(NoticeStatus noticeStatus, NoticeRequest request) {
        Notice notice = new Notice.NoticeBuilder(request, noticeStatus).build();
        noticeRepository.save(notice);
    }

    /**
     * 홈 화면용 공지사항 정보
     *
     * @return
     */
    public ListResult<NoticeHomeItem> getTopNotice() {
        List<Notice> notices = noticeRepository.findTop5ByOrderByIdDesc();
        List<NoticeHomeItem> result = new LinkedList<>();
        notices.forEach(notice -> {
            NoticeHomeItem noticeHomeItem = new NoticeHomeItem.NoticeHomeItemBuilder(notice).build();
            result.add(noticeHomeItem);
        });

        return ListConvertService.settingResult(result);
    }

    /**
     * 공지사항 상세정보 조회
     *
     * @return
     */
    public ListResult<NoticeDetailItem> getNoticeDetail() {
        List<Notice> notices = noticeRepository.findAll();
        List<NoticeDetailItem> result = new LinkedList<>();
        notices.forEach(notice -> {
            NoticeDetailItem noticeDetailItem = new NoticeDetailItem.NoticeDetailItemBuilder(notice).build();
            result.add(noticeDetailItem);
        });

        return ListConvertService.settingResult(result);
    }

    /**
     * 공지사항 정보수정 (제목, 내용, 작성자)
     *
     * @param noticeId
     * @param request
     */
    public void putNotice(long noticeId, PutNoticeRequest request) {
        Notice notice = noticeRepository.findById(noticeId).orElseThrow(CMissingDataException::new);
        notice.putNotice(request);
        noticeRepository.save(notice);
    }

    /**
     * 공지사항 게시기간 수정
     *
     * @param noticeId
     * @param request
     */
    public void putNoticeDate(long noticeId, PutNoticeDateRequest request) {
        Notice notice = noticeRepository.findById(noticeId).orElseThrow(CMissingDataException::new);
        notice.putNoticeDate(request);
        noticeRepository.save(notice);
    }

    /**
     * 공지사항 상태 수정
     *
     * @param noticeId
     * @param putNoticeStatus
     */
    public void putNoticeStatus(long noticeId, PutNoticeStatus putNoticeStatus) {
        Notice notice = noticeRepository.findById(noticeId).orElseThrow(CMissingDataException::new);
        notice.putNoticeStatus(putNoticeStatus);
        noticeRepository.save(notice);
    }

    /**
     * 공지사항 삭제
     *
     * @param noticeId
     */
    public void delNotice(long noticeId) {
        noticeRepository.deleteById(noticeId);
    }

}

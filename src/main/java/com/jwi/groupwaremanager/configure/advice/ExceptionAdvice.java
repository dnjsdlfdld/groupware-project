package com.jwi.groupwaremanager.configure.advice;

import com.jwi.groupwaremanager.enums.ResultCode;
import com.jwi.groupwaremanager.exception.*;
import com.jwi.groupwaremanager.model.common.CommonResult;
import com.jwi.groupwaremanager.service.ResponseService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;


@RestControllerAdvice
public class ExceptionAdvice {
    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult defaultException(HttpServletRequest request, Exception e) {
        return ResponseService.getFailResult(ResultCode.FAILED);
    }

    @ExceptionHandler(CMissingDataException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CMissingDataException e) {
        return ResponseService.getFailResult(ResultCode.MISSING_DATA);
    }

    @ExceptionHandler(CNoDataException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult defaultException(HttpServletRequest request, CNoDataException e) {
        return ResponseService.getFailResult(ResultCode.NO_DATA);
    }

    // 퇴사했거나 등록되지 않은 사원
    @ExceptionHandler(CNoInformationException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult defaultException(HttpServletRequest request, CNoInformationException e) {
        return ResponseService.getFailResult(ResultCode.NO_INFORMATION);
    }

    // 이미 출근 처리함
    @ExceptionHandler(CAlreadyAtWorkException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult defaultException(HttpServletRequest request, CAlreadyAtWorkException e) {
        return ResponseService.getFailResult(ResultCode.ALREADY_AT_WORK);
    }

    // 출근 처리를 해주셈
    @ExceptionHandler(CNoCheckWorkException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult defaultException(HttpServletRequest request, CNoCheckWorkException e) {
        return ResponseService.getFailResult(ResultCode.NO_CHECK_WORK);
    }

    // 이미 퇴근 처리됨
    @ExceptionHandler(CAlreadyLeaveWorkException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult defaultException(HttpServletRequest request, CAlreadyLeaveWorkException e) {
        return ResponseService.getFailResult(ResultCode.ALREADY_LEAVE_WORK);
    }

    // 같은 상태로 변경할 수 없습니다
    @ExceptionHandler(CNoSameDataException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult defaultException(HttpServletRequest request, CNoSameDataException e) {
        return ResponseService.getFailResult(ResultCode.NO_SAME_DATA);
    }

    // 이미 조퇴 처리됨
    @ExceptionHandler(CAlreadyEarlyLeaveWorkException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult defaultException(HttpServletRequest request, CAlreadyEarlyLeaveWorkException e) {
        return ResponseService.getFailResult(ResultCode.ALREADY_EARLY_LEAVE_WORK);
    }

    // 패스워드가 일치하지 않다
    @ExceptionHandler(CDoNotPasswordException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult defaultException(HttpServletRequest request, CDoNotPasswordException e) {
        return ResponseService.getFailResult(ResultCode.DO_NOT_PASSWORD);
    }

    // 계정이 유효하지 않다.
    @ExceptionHandler(CValidNotAccountException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult defaultException(HttpServletRequest request, CValidNotAccountException e) {
        return ResponseService.getFailResult(ResultCode.VALID_NOT_ACCOUNT);
    }

    // 퇴사한 직원
    @ExceptionHandler(CResignCompanyException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult defaultException(HttpServletRequest request, CResignCompanyException e) {
        return ResponseService.getFailResult(ResultCode.RESIGN_COMPANY);
    }

    // 보유한 연차일 수가 부족할 때
    @ExceptionHandler(CCheckHolidayException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult defaultException(HttpServletRequest request, CCheckHolidayException e) {
        return ResponseService.getFailResult(ResultCode.CHECK_HOLIDAY);
    }

    // 이미 반려 처리된 결재다
    @ExceptionHandler(CAlreadyReturnApprovalException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult defaultException(HttpServletRequest request, CAlreadyReturnApprovalException e) {
        return ResponseService.getFailResult(ResultCode.ALREADY_RETURN_APPROVAL);
    }

    @ExceptionHandler(CNoAttendanceRecordException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult defaultException(HttpServletRequest request, CNoAttendanceRecordException e) {
        return ResponseService.getFailResult(ResultCode.NO_ATTENDANCE_RECORD);
    }

    @ExceptionHandler(COverlapUsernameException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult defaultException(HttpServletRequest request, COverlapUsernameException e) {
        return ResponseService.getFailResult(ResultCode.OVERLAP_USERNAME);
    }

    @ExceptionHandler(CDoNotPinPasswordException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult defaultException(HttpServletRequest request, CDoNotPinPasswordException e) {
        return ResponseService.getFailResult(ResultCode.DO_NOT_PIN_PASSWORD);
    }
}

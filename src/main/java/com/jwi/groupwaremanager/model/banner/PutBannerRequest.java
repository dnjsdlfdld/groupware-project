package com.jwi.groupwaremanager.model.banner;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class PutBannerRequest {
    @NotNull
    @Length(min = 1, max = 150)
    @ApiModelProperty(notes = "이미지주소")
    private String imageUrl;

    @NotNull
    @Length(min = 1, max = 20)
    @ApiModelProperty(notes = "제목")
    private String title;
}

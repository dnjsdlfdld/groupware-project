package com.jwi.groupwaremanager.model.banner;

import com.jwi.groupwaremanager.entity.Banner;
import com.jwi.groupwaremanager.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class BannerHomeItem {
    @ApiModelProperty(notes = "이미지주소")
    private String imageUrl;

    private BannerHomeItem(BannerItemBuilder builder) {
        this.imageUrl = builder.imageUrl;
    }

    public static class BannerItemBuilder implements CommonModelBuilder<BannerHomeItem> {

        private final String imageUrl;

        public BannerItemBuilder(Banner banner) {
            this.imageUrl = banner.getImageUrl();
        }

        @Override
        public BannerHomeItem build() {
            return new BannerHomeItem(this);
        }
    }
}

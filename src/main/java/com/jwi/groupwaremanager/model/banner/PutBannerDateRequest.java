package com.jwi.groupwaremanager.model.banner;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
public class PutBannerDateRequest {
    @NotNull
    @ApiModelProperty(notes = "게시시작일")
    private LocalDate postStart;

    @NotNull
    @ApiModelProperty(notes = "게시종료일")
    private LocalDate postEnd;
}

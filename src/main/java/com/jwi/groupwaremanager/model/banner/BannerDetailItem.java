package com.jwi.groupwaremanager.model.banner;

import com.jwi.groupwaremanager.entity.Banner;
import com.jwi.groupwaremanager.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class BannerDetailItem {
    @ApiModelProperty(notes = "배너 등록번호")
    private Long id;
    @ApiModelProperty(notes = "제목")
    private String title;
    @ApiModelProperty(notes = "게시시작일")
    private LocalDate postStart;
    @ApiModelProperty(notes = "게시종료일")
    private LocalDate postEnd;
    @ApiModelProperty(notes = "등록일자")
    private LocalDateTime dateCreate;

    private BannerDetailItem(BannerDetailItemBuilder builder) {
        this.id = builder.id;
        this.title = builder.title;
        this.postStart = builder.postStart;
        this.postEnd = builder.postEnd;
        this.dateCreate = builder.dateCreate;
    }

    public static class BannerDetailItemBuilder implements CommonModelBuilder<BannerDetailItem> {

        private final Long id;
        private final String title;
        private final LocalDate postStart;
        private final LocalDate postEnd;
        private final LocalDateTime dateCreate;

        public BannerDetailItemBuilder(Banner banner) {
            this.id = banner.getId();
            this.title = banner.getTitle();
            this.postStart = banner.getPostStart();
            this.postEnd = banner.getPostEnd();
            this.dateCreate = banner.getDateCreate();
        }

        @Override
        public BannerDetailItem build() {
            return new BannerDetailItem(this);
        }
    }
}

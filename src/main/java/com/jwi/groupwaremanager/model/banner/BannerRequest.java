package com.jwi.groupwaremanager.model.banner;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
public class BannerRequest {
    @NotNull
    @Length(min = 1, max = 150)
    @ApiModelProperty(notes = "이미지 링크")
    private String imageUrl;

    @NotNull
    @Length(min = 1, max = 150)
    @ApiModelProperty(notes = "제목")
    private String title;

    @NotNull
    @ApiModelProperty(notes = "게시시작일")
    private LocalDate postStart;

    @NotNull
    @ApiModelProperty(notes = "게시종료일")
    private LocalDate postEnd;
}

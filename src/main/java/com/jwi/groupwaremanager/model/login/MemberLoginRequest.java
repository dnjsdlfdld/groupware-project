package com.jwi.groupwaremanager.model.login;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class MemberLoginRequest {

    @NotNull
    @Length(min = 5, max = 20)
    @ApiModelProperty(notes = "아이디", required = true)
    private String username;

    @NotNull
    @Length(min = 8, max = 20)
    @ApiModelProperty(notes = "비밀번호", required = true)
    private String password;
}

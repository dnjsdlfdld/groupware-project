package com.jwi.groupwaremanager.model.login;

import com.jwi.groupwaremanager.entity.Member;
import com.jwi.groupwaremanager.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class LoginResponse {
    @ApiModelProperty(notes = "사원 등록번호")
    private Long memberId;

    @ApiModelProperty(notes = "아이디")
    private String name;

    @ApiModelProperty(notes = "로그인 시간")
    private LocalDateTime loginTime;

    private LoginResponse(MemberLoginResponseBuilder builder) {
        this.memberId = builder.memberId;
        this.name = builder.name;
        this.loginTime = builder.loginTime;
    }

    public static class MemberLoginResponseBuilder implements CommonModelBuilder<LoginResponse> {

        private final Long memberId;
        private final String name;
        private final LocalDateTime loginTime;

        public MemberLoginResponseBuilder(Member member) {
            this.memberId = member.getId();
            this.name = member.getName();
            this.loginTime = member.getLoginTime();
        }

        @Override
        public LoginResponse build() {
            return new LoginResponse(this);
        }
    }

    private LoginResponse(MemberLoginResponseInfoBuilder builder) {
        this.name = builder.name;
        this.loginTime = builder.loginTime;
    }
    public static class MemberLoginResponseInfoBuilder implements CommonModelBuilder<LoginResponse> {
        private final String name;
        private final LocalDateTime loginTime;

        public MemberLoginResponseInfoBuilder(Member member) {
            this.name = member.getName();
            this.loginTime = member.getLoginTime();
        }
        @Override
        public LoginResponse build() {
            return new LoginResponse(this);
        }
    }

}

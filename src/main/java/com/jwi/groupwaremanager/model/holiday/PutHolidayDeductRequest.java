package com.jwi.groupwaremanager.model.holiday;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class PutHolidayDeductRequest {
    @NotNull
    @ApiModelProperty(notes = "사용일 수")
    private Double availableHoliday;

    @NotNull
    @ApiModelProperty(notes = "잔여일 수")
    private Double useHoliday;
}

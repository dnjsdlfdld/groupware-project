package com.jwi.groupwaremanager.model.holiday;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class HolidayManageRequest {
    @NotNull
    @ApiModelProperty(notes = "총 할당일 수", required = true)
    private Double allocationHoliday;

    @NotNull
    @ApiModelProperty(notes = "사용일 수", required = true)
    private Double useHoliday;
}

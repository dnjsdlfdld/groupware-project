package com.jwi.groupwaremanager.model.holiday;

import com.jwi.groupwaremanager.entity.HolidayUpload;
import com.jwi.groupwaremanager.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class HolidaySimpleList {
    @ApiModelProperty(notes = "결재 등록번호")
    private Long uploadId;
    @ApiModelProperty(notes = "직원명 + 부서명")
    private String memberFullName;
    @ApiModelProperty(notes = "제목")
    private String title;

    private HolidaySimpleList(HolidaySimpleListBuilder builder) {
        this.uploadId = builder.uploadId;
        this.memberFullName = builder.memberFullName;
        this.title = builder.title;
    }

    public static class HolidaySimpleListBuilder implements CommonModelBuilder<HolidaySimpleList> {
        private final Long uploadId;
        private final String memberFullName;
        private final String title;

        public HolidaySimpleListBuilder(HolidayUpload holidayUpload) {
            this.uploadId = holidayUpload.getId();
            this.memberFullName = holidayUpload.getMember().getName() + " [" + holidayUpload.getMember().getDepartment() + "]";
            this.title = holidayUpload.getTitle();
        }
        @Override
        public HolidaySimpleList build() {
            return new HolidaySimpleList(this);
        }
    }
}

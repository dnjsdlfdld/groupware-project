package com.jwi.groupwaremanager.model.holiday;

import com.jwi.groupwaremanager.enums.holiday.HolidayType;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Getter
@Setter
public class HolidayUserRequest {
    @NotNull
    @Length(min = 5, max = 20)
    @ApiModelProperty(notes = "제목")
    private String title;

    @NotNull
    @Length(min = 1, max = 50)
    @ApiModelProperty(notes = "내용")
    private String content;

    @NotNull
    @Enumerated(EnumType.STRING)
    @ApiModelProperty(notes = "휴가타입")
    private HolidayType holidayType;

    @NotNull
    @ApiModelProperty(notes = "휴가 시작일")
    private LocalDateTime startHoliday;

    @NotNull
    @ApiModelProperty(notes = "휴가 종료일")
    private LocalDateTime endHoliday;

    @NotNull
    @ApiModelProperty(notes = "사용일수")
    private Float useHoliday;
}

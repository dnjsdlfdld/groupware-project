package com.jwi.groupwaremanager.model.holiday;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class PutGiveHolidayRequest {
    @NotNull
    @ApiModelProperty(value = "총 할당일 수")
    private Double allocationHoliday;

    @NotNull
    @ApiModelProperty(value = "잔여일 수")
    private Double useHoliday;
}

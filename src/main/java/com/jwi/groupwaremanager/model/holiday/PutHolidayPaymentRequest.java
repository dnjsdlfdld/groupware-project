package com.jwi.groupwaremanager.model.holiday;

import com.jwi.groupwaremanager.enums.holiday.ApprovalState;
import com.jwi.groupwaremanager.enums.holiday.HolidayType;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Getter
@Setter
public class PutHolidayPaymentRequest {
    @NotNull
    @ApiModelProperty(notes = "휴가구분")
    private HolidayType holidayType;

    @NotNull
    @ApiModelProperty(notes = "휴가 시작일자")
    private LocalDateTime startHoliday;

    @NotNull
    @ApiModelProperty(notes = "휴가 종료일자")
    private LocalDateTime endHoliday;

    @NotNull
    @ApiModelProperty(notes = "결재 상태")
    private ApprovalState approvalState;
}

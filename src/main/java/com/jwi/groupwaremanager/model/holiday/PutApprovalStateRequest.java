package com.jwi.groupwaremanager.model.holiday;

import com.jwi.groupwaremanager.enums.holiday.ApprovalState;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class PutApprovalStateRequest {
    @NotNull
    @ApiModelProperty(value = "결재상태")
    private ApprovalState approvalState;
}

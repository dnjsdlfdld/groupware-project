package com.jwi.groupwaremanager.model.holiday;

import com.jwi.groupwaremanager.entity.HolidayUpload;
import com.jwi.groupwaremanager.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class HolidayUploadItem {
    @ApiModelProperty(notes = "결재 등록번호")
    private Long uploadId;
    @ApiModelProperty(notes = "직원명 + 부서명")
    private String memberFullName;
    @ApiModelProperty(notes = "제목")
    private String title;
    @ApiModelProperty(notes = "내용")
    private String content;
    @ApiModelProperty(notes = "휴가타입")
    private String holidayType;
    @ApiModelProperty(notes = "휴가 시작일")
    private LocalDateTime startHoliday;
    @ApiModelProperty(notes = "휴가 종료일")
    private LocalDateTime endHoliday;
    @ApiModelProperty(notes = "사용일수")
    private String useHoliday;
    @ApiModelProperty(notes = "승인상태")
    private String approvalState;

    @ApiModelProperty(notes = "승인시간 > 결재완료 때만 작동")
    private String approvalDate;

    @ApiModelProperty(notes = "반려시간 > 반려처리 때만 작동")
    private String companionDate;

    private HolidayUploadItem(HolidayUploadItemBuilder builder) {
        this.uploadId = builder.uploadId;
        this.memberFullName = builder.memberFullName;
        this.title = builder.title;
        this.content = builder.content;
        this.holidayType = builder.holidayType;
        this.startHoliday = builder.startHoliday;
        this.endHoliday = builder.endHoliday;
        this.useHoliday = builder.useHoliday;
        this.approvalDate = builder.approvalDate;
        this.approvalState = builder.approvalState;
        this.companionDate = builder.companionDate;
    }

    public static class HolidayUploadItemBuilder implements CommonModelBuilder<HolidayUploadItem> {
        private final Long uploadId;
        private final String memberFullName;
        private final String title;
        private final String content;
        private final String holidayType;
        private final LocalDateTime startHoliday;
        private final LocalDateTime endHoliday;
        private final String useHoliday;
        private final String approvalState;
        private final String approvalDate;
        private final String companionDate;

        public HolidayUploadItemBuilder(HolidayUpload holidayUpload) {
            this.uploadId = holidayUpload.getId();
            this.memberFullName = holidayUpload.getMember().getName() + " [" + holidayUpload.getMember().getDepartment() + "]";
            this.title = holidayUpload.getTitle();
            this.content = holidayUpload.getContent();
            this.holidayType = holidayUpload.getHolidayType().getName();
            this.startHoliday = holidayUpload.getStartHoliday();
            this.endHoliday = holidayUpload.getEndHoliday();
            this.useHoliday = holidayUpload.getUseHoliday() + "일";
            this.approvalState = holidayUpload.getApprovalState().getName();
            this.approvalDate = holidayUpload.getApprovalDate() == null ? "-" : holidayUpload.getApprovalDate().toString();
            this.companionDate = holidayUpload.getCompanionDate() == null ? "-" : holidayUpload.getCompanionDate().toString();
        }

        @Override
        public HolidayUploadItem build() {
            return new HolidayUploadItem(this);
        }
    }
}

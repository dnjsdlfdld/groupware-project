package com.jwi.groupwaremanager.model.attendance;

import com.jwi.groupwaremanager.enums.attendance.AttendanceType;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class AttendanceUserRequest {
    @NotNull
    @Enumerated(EnumType.STRING)
    @ApiModelProperty(value = "출퇴근 여부")
    private AttendanceType attendanceType;
}

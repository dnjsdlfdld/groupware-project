package com.jwi.groupwaremanager.model.attendance;

import com.jwi.groupwaremanager.entity.Attendance;
import com.jwi.groupwaremanager.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalTime;

@Getter
@NoArgsConstructor
public class AttendanceListItem {
    private Long memberId;
    @ApiModelProperty(notes = "등록번호")
    private String memberName;

    @ApiModelProperty(notes = "현재상태")
    private String attendanceType;

    @ApiModelProperty(notes = "출근시간")
    private String timeStart;

    @ApiModelProperty(notes = "퇴근시간")
    private String timeLeave;

    @ApiModelProperty(notes = "조퇴시간")
    private String timeEarlyLeave;


    private AttendanceListItem(AttendanceListItemBuilder builder) {
        this.memberId = builder.memberId;
        this.memberName = builder.memberName;
        this.attendanceType = builder.attendanceType;
        this.timeStart = builder.timeStart;
        this.timeLeave = builder.timeLeave;
        this.timeEarlyLeave = builder.timeEarlyLeave;
    }

    public static class AttendanceListItemBuilder implements CommonModelBuilder<AttendanceListItem> {
        private final Long memberId;
        private final String memberName;
        private final String attendanceType;
        private final String timeStart;
        private final String timeLeave;
        private final String timeEarlyLeave;

        public AttendanceListItemBuilder(Attendance attendance) {
            this.memberId = attendance.getMember().getId();
            this.memberName = attendance.getMember().getName();
            this.attendanceType = attendance.getAttendanceType().getName();
            this.timeStart = attendance.getTimeStart() == null ? "-" : attendance.getTimeStart().toString();
            this.timeLeave = attendance.getTimeLeave() == null ? "-" : attendance.getTimeLeave().toString();
            this.timeEarlyLeave = attendance.getTimeEarlyLeave() == null ? "-" : attendance.getTimeEarlyLeave().toString();
    }

        @Override
        public AttendanceListItem build() {
            return new AttendanceListItem(this);
        }
    }
}

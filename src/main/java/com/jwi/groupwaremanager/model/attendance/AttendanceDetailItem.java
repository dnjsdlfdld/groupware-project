package com.jwi.groupwaremanager.model.attendance;

import com.jwi.groupwaremanager.entity.Attendance;
import com.jwi.groupwaremanager.interfaces.CommonModelBuilder;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalTime;

@Getter
@Setter
public class AttendanceDetailItem {
    private Long memberId;
    private String memberName;
    private LocalDate dateWork;
    private String attendanceType;
    private LocalTime timeStart;
    private LocalTime timeLeave;
    private LocalTime timeEarlyLeave;

    private AttendanceDetailItem(AttendanceDetailItemBuilder builder) {
        this.memberId = builder.memberId;
        this.memberName = builder.memberName;
        this.dateWork = builder.dateWork;
        this.attendanceType = builder.attendanceType;
        this.timeStart = builder.timeStart;
        this.timeLeave = builder.timeLeave;
        this.timeEarlyLeave = builder.timeEarlyLeave;
    }

    public static class AttendanceDetailItemBuilder implements CommonModelBuilder<AttendanceDetailItem> {
        private final Long memberId;
        private final String memberName;
        private final LocalDate dateWork;
        private final String attendanceType;
        private final LocalTime timeStart;
        private final LocalTime timeLeave;
        private final LocalTime timeEarlyLeave;

        public AttendanceDetailItemBuilder(Attendance attendance) {
            this.memberId = attendance.getMember().getId();
            this.memberName = attendance.getMember().getName();
            this.dateWork = attendance.getDateWork();
            this.attendanceType = attendance.getAttendanceType().getName();
            this.timeStart = attendance.getTimeStart();
            this.timeLeave = attendance.getTimeLeave();
            this.timeEarlyLeave = attendance.getTimeEarlyLeave();
        }

        @Override
        public AttendanceDetailItem build() {
            return new AttendanceDetailItem(this);
        }
    }
}

package com.jwi.groupwaremanager.model.attendance;

import com.jwi.groupwaremanager.entity.Attendance;
import com.jwi.groupwaremanager.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class AttendanceTypeResponse {
    @ApiModelProperty(value = "근무상태")
    @Enumerated(EnumType.STRING)
    private String attendanceType;

    private AttendanceTypeResponse(AttendanceTypeResponseBuilder builder) {
        this.attendanceType = builder.attendanceType;
    }
    // 상태가 없는 경우 보여지는 빌더
    public static class AttendanceTypeResponseBuilder implements CommonModelBuilder<AttendanceTypeResponse> {

        private final String attendanceType;

        public AttendanceTypeResponseBuilder(Attendance attendance) {
            this.attendanceType = attendance.getAttendanceType().getName();
        }

        @Override
        public AttendanceTypeResponse build() {
            return new AttendanceTypeResponse(this);
        }
    }
}

package com.jwi.groupwaremanager.model.attendance;

import com.jwi.groupwaremanager.entity.Attendance;
import com.jwi.groupwaremanager.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class AttendanceUserItem {
    @ApiModelProperty(value = "출근시간")
    private LocalTime dateAttendance;

    @ApiModelProperty(value = "퇴근시간")
    private LocalTime leaveTime;

    @ApiModelProperty(value = "조퇴시간")
    private LocalTime earlyLeaveTime;

    private AttendanceUserItem(AttendanceUserItemBuilder builder) {
        this.dateAttendance = builder.dateAttendance;
        this.leaveTime = builder.leaveTime;
        this.earlyLeaveTime = builder.earlyLeaveTime;
    }

    public static class AttendanceUserItemBuilder implements CommonModelBuilder<AttendanceUserItem> {

        private final LocalTime dateAttendance;
        private final LocalTime leaveTime;
        private final LocalTime earlyLeaveTime;

        public AttendanceUserItemBuilder(Attendance attendance) {
            this.dateAttendance = attendance.getTimeStart();
            this.leaveTime = attendance.getTimeLeave();
            this.earlyLeaveTime = attendance.getTimeEarlyLeave();
        }

        @Override
        public AttendanceUserItem build() {
            return new AttendanceUserItem(this);
        }
    }
}

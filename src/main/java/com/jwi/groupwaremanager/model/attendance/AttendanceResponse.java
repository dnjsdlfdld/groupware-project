package com.jwi.groupwaremanager.model.attendance;

import com.jwi.groupwaremanager.entity.Attendance;
import com.jwi.groupwaremanager.enums.attendance.AttendanceType;
import com.jwi.groupwaremanager.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class AttendanceResponse {
    // 한국어로.. 출근/조퇴/퇴근
    private String attendanceTypeName;

    // ENUM - key값
    private String attendanceType;

    private AttendanceResponse(AttendanceResponseBuilder builder) {
        this.attendanceTypeName = builder.attendanceTypeName;
        this.attendanceType = builder.attendanceType;
    }

    public static class AttendanceResponseBuilder implements CommonModelBuilder<AttendanceResponse> {
        private final String attendanceTypeName;
        private final String attendanceType;


        public AttendanceResponseBuilder(Attendance attendanceTest) {
            this.attendanceTypeName = attendanceTest.getAttendanceType().getName();
            this.attendanceType = attendanceTest.getAttendanceType().toString();
        }

        @Override
        public AttendanceResponse build() {
            return new AttendanceResponse(this);
        }
    }

    private AttendanceResponse(AttendanceResponseNoneBuilder builder) {
        this.attendanceTypeName = builder.attendanceTypeName;
        this.attendanceType = builder.attendanceType;
    }

    public static class AttendanceResponseNoneBuilder implements CommonModelBuilder<AttendanceResponse> {
        private final String attendanceTypeName;
        private final String attendanceType;

        public AttendanceResponseNoneBuilder() {
            this.attendanceTypeName = AttendanceType.NONE.getName();
            this.attendanceType = AttendanceType.NONE.toString();
        }

        @Override
        public AttendanceResponse build() {
            return new AttendanceResponse(this);
        }
    }
}

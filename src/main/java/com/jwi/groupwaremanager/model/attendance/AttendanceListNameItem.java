package com.jwi.groupwaremanager.model.attendance;

import com.jwi.groupwaremanager.entity.Member;
import com.jwi.groupwaremanager.interfaces.CommonModelBuilder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AttendanceListNameItem {
    private String memberName;

    private AttendanceListNameItem(AttendanceListNameItemBuilder builder) {
        this.memberName = builder.memberName;
    }

    public static class AttendanceListNameItemBuilder implements CommonModelBuilder<AttendanceListNameItem> {
        private final String memberName;

        public AttendanceListNameItemBuilder(Member member) {
            this.memberName = member.getName();
        }

        @Override
        public AttendanceListNameItem build() {
            return new AttendanceListNameItem(this);
        }
    }
}

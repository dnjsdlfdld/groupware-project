package com.jwi.groupwaremanager.model.notice;

import com.jwi.groupwaremanager.entity.Notice;
import com.jwi.groupwaremanager.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class NoticeHomeItem {
    @ApiModelProperty(notes = "제목")
    private String noticeTitle;


    private NoticeHomeItem(NoticeHomeItemBuilder builder) {
        this.noticeTitle = builder.noticeTitle;
    }

    public static class NoticeHomeItemBuilder implements CommonModelBuilder<NoticeHomeItem> {
        private final String noticeTitle;

        public NoticeHomeItemBuilder(Notice notice) {
            this.noticeTitle = notice.getTitle();
        }

        @Override
        public NoticeHomeItem build() {
            return new NoticeHomeItem(this);
        }
    }
}

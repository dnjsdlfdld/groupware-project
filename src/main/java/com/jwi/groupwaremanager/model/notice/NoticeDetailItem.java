package com.jwi.groupwaremanager.model.notice;

import com.jwi.groupwaremanager.entity.Notice;
import com.jwi.groupwaremanager.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class NoticeDetailItem {
    @ApiModelProperty(notes = "제목")
    private String title;

    @ApiModelProperty(notes = "내용")
    private String content;

    @ApiModelProperty(notes = "작성자")
    private String writer;

    @ApiModelProperty(notes = "등록일자")
    private LocalDateTime dateCreate;

    private NoticeDetailItem(NoticeDetailItemBuilder builder) {
        this.title = builder.title;
        this.content = builder.content;
        this.writer = builder.writer;
        this.dateCreate = builder.dateCreate;
    }

    public static class NoticeDetailItemBuilder implements CommonModelBuilder<NoticeDetailItem> {

        private final String title;
        private final String content;
        private final String writer;
        private final LocalDateTime dateCreate;

        public NoticeDetailItemBuilder(Notice notice) {
            this.title = notice.getTitle();
            this.content = notice.getContent();
            this.writer = notice.getWriter();
            this.dateCreate = notice.getDateCreate();
        }

        @Override
        public NoticeDetailItem build() {
            return new NoticeDetailItem(this);
        }
    }
}

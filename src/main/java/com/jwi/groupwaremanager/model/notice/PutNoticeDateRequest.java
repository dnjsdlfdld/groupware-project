package com.jwi.groupwaremanager.model.notice;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
public class PutNoticeDateRequest {
    @NotNull
    @ApiModelProperty(notes = "게시 시작일")
    private LocalDate noticeStart;

    @NotNull
    @ApiModelProperty(notes = "게시 종료일")
    private LocalDate noticeEnd;
}

package com.jwi.groupwaremanager.model.notice;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
public class NoticeRequest {
    @NotNull
    @Length(min = 1, max = 20)
    @ApiModelProperty(notes = "제목", required = true)
    private String title;

    @NotNull
    @Length(min = 1, max = 200)
    @ApiModelProperty(notes = "내용", required = true)
    private String content;

    @NotNull
    @Length(min = 2, max = 20)
    @ApiModelProperty(notes = "작성자")
    private String writer;

    @NotNull
    @ApiModelProperty(notes = "게시시작일", required = true)
    private LocalDate noticeStart;

    @NotNull
    @ApiModelProperty(notes = "게시종료일", required = true)
    private LocalDate noticeEnd;
}

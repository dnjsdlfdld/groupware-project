package com.jwi.groupwaremanager.model.notice;

import com.jwi.groupwaremanager.enums.notice.NoticeStatus;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class PutNoticeRequest {
    @NotNull
    @Length(min = 1, max = 20)
    @ApiModelProperty(notes = "제목")
    private String title;

    @NotNull
    @Length(min = 1, max = 200)
    @ApiModelProperty(notes = "내용")
    private String content;

    @NotNull
    @Length(min = 2, max = 20)
    @ApiModelProperty(notes = "작성자")
    private String writer;
}

package com.jwi.groupwaremanager.model.notice;

import com.jwi.groupwaremanager.enums.notice.NoticeStatus;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class PutNoticeStatus {
    @NotNull
    @Enumerated(EnumType.STRING)
    @ApiModelProperty(notes = "공지 상태")
    private NoticeStatus noticeStatus;
}

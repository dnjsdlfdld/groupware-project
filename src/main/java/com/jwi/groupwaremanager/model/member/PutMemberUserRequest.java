package com.jwi.groupwaremanager.model.member;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class PutMemberUserRequest {
    @NotNull
    @Length(min = 13, max = 13)
    @ApiModelProperty(value = "연락처", required = true)
    private String phone;

    @NotNull
    @Length(min = 10, max = 50)
    @ApiModelProperty(value = "주소", required = true)
    private String address;
}

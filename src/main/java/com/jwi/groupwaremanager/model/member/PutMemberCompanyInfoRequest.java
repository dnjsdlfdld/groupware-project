package com.jwi.groupwaremanager.model.member;

import com.jwi.groupwaremanager.enums.member.Position;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class PutMemberCompanyInfoRequest {
    @NotNull
    @Length(min = 3, max = 10)
    @ApiModelProperty(value = "부서", required = true)
    private String department;

    @NotNull
    @Enumerated(EnumType.STRING)
    @ApiModelProperty(value = "직급", required = true)
    private Position position;

}

package com.jwi.groupwaremanager.model.member;

import com.jwi.groupwaremanager.enums.member.Position;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class PutMemberPositionRequest {
    @NotNull
    @ApiModelProperty(notes = "직급")
    public Position position;
}

package com.jwi.groupwaremanager.model.member;

import com.jwi.groupwaremanager.entity.Member;
import com.jwi.groupwaremanager.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MemberUserItem {
    @ApiModelProperty(notes = "아이디")
    private String username;

    @ApiModelProperty(notes = "이름")
    private String name;

    @ApiModelProperty(notes = "생년월일")
    private LocalDate birth;

    @ApiModelProperty(notes = "연락처")
    private String phone;

    @ApiModelProperty(notes = "주소")
    private String address;

    @ApiModelProperty(notes = "부서")
    private String department;

    @ApiModelProperty(notes = "직급")
    private String position;

    @ApiModelProperty(notes = "입사일")
    private LocalDate dateIn;

    private MemberUserItem(MemberUserItemBuilder builder) {
        this.username = builder.username;
        this.name = builder.name;
        this.birth = builder.birth;
        this.phone = builder.phone;
        this.address = builder.address;
        this.department = builder.department;
        this.position = builder.position;
        this.dateIn = builder.dateIn;
    }

    public static class MemberUserItemBuilder implements CommonModelBuilder<MemberUserItem> {

        private final String username;
        private final String name;
        private final LocalDate birth;
        private final String phone;
        private final String address;
        private final String department;
        private final String position;
        private final LocalDate dateIn;

        public MemberUserItemBuilder(Member member) {
            this.username = member.getUsername();
            this.name = member.getName();
            this.birth = member.getBirth();
            this.phone = member.getPhone();
            this.address = member.getAddress();
            this.department = member.getDepartment();
            this.position = member.getPosition().getName();
            this.dateIn = member.getDateIn();
        }

        @Override
        public MemberUserItem build() {
            return new MemberUserItem(this);
        }
    }
}

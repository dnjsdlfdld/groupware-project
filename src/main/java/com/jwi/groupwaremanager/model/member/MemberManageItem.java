package com.jwi.groupwaremanager.model.member;

import com.jwi.groupwaremanager.entity.Member;
import com.jwi.groupwaremanager.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MemberManageItem {
    private Long id;
    private String name;
    private LocalDate dateIn;

    private MemberManageItem(MemberManageItemBuilder builder) {
        this.id = builder.id;
        this.name = builder.name;
        this.dateIn = builder.dateIn;
    }

    public static class MemberManageItemBuilder implements CommonModelBuilder<MemberManageItem> {
        private final Long id;
        private final String name;
        private final LocalDate dateIn;

        public MemberManageItemBuilder(Member member) {
            this.id = member.getId();
            this.name = member.getName();
            this.dateIn = member.getDateIn();
        }

        @Override
        public MemberManageItem build() {
            return new MemberManageItem(this);
        }
    }
}

package com.jwi.groupwaremanager.model.member;

import com.jwi.groupwaremanager.enums.member.Position;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;


@Getter
@Setter
public class PutMemberRequest {
    @NotNull
    @Length(min = 2, max = 20)
    @ApiModelProperty(notes = "이름", required = true)
    private String name;

    @NotNull
    @Length(min = 13, max = 13)
    @ApiModelProperty(value = "연락처", required = true)
    private String phone;

    @NotNull
    @Length(min = 10, max = 50)
    @ApiModelProperty(value = "주소", required = true)
    private String address;

//    @NotNull
//    @Length(min = 3, max = 10)
//    @ApiModelProperty(value = "부서", required = true)
//    private String department;
//
//    @NotNull
//    @Enumerated(EnumType.STRING)
//    @ApiModelProperty(value = "직급", required = true)
//    private Position position;

}

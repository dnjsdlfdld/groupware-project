package com.jwi.groupwaremanager.model.member;

import com.jwi.groupwaremanager.entity.Member;
import com.jwi.groupwaremanager.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MemberManagerItemDetail {
    @ApiModelProperty(notes = "직원 등록번호")
    private Long id;

    @ApiModelProperty(notes = "아이디")
    private String username;

    @ApiModelProperty(notes = "비밀번호")
    private String password;

    @ApiModelProperty(notes = "이름")
    private String name;

    @ApiModelProperty(notes = "생년월일")
    private LocalDate birth;

    @ApiModelProperty(notes = "연락처")
    private String phone;

    @ApiModelProperty(notes = "주소")
    private String address;

    @ApiModelProperty(notes = "부서")
    private String department;

    @ApiModelProperty(notes = "직급")
    private String position;

    @ApiModelProperty(notes = "권한")
    private Boolean isAdmin;

    @ApiModelProperty(notes = "입사일")
    private LocalDate dateIn;

    @ApiModelProperty(notes = "근무여부")
    private Boolean isEnable;

    @ApiModelProperty(notes = "퇴사일")
    private LocalDate dateOut;

    @ApiModelProperty(notes = "등록일자")
    private LocalDateTime dateCreate;

    @ApiModelProperty(notes = "수정일자")
    private LocalDateTime dateUpdate;

    private MemberManagerItemDetail(MemberManagerItemBuilder builder) {
        this.id = builder.id;
        this.username = builder.username;
        this.password = builder.password;
        this.name = builder.name;
        this.birth = builder.birth;
        this.phone = builder.phone;
        this.address = builder.address;
        this.department = builder.department;
        this.position = builder.position;
        this.isAdmin = builder.isAdmin;
        this.dateIn = builder.dateIn;
        this.isEnable = builder.isEnable;
        this.dateOut = builder.dateOut;
        this.dateCreate = builder.dateCreate;
        this.dateUpdate = builder.dateUpdate;
    }

    public static class MemberManagerItemBuilder implements CommonModelBuilder<MemberManagerItemDetail> {

        private final Long id;
        private final String username;
        private final String password;
        private final String name;
        private final LocalDate birth;
        private final String phone;
        private final String address;
        private final String department;
        private final String position;
        private final Boolean isAdmin;
        private final LocalDate dateIn;
        private final Boolean isEnable;
        private final LocalDate dateOut;
        private final LocalDateTime dateCreate;
        private final LocalDateTime dateUpdate;

        public MemberManagerItemBuilder(Member member) {
            this.id = member.getId();
            this.username = member.getUsername();
            this.password = member.getPassword();
            this.name = member.getName();
            this.birth = member.getBirth();
            this.phone = member.getPhone();
            this.address = member.getAddress();
            this.department = member.getDepartment();
            this.position = member.getPosition().getName();
            this.isAdmin = member.getIsAdmin();
            this.dateIn = member.getDateIn();
            this.isEnable = member.getIsEnable();
            this.dateOut = member.getDateOut();
            this.dateCreate = member.getDateCreate();
            this.dateUpdate = member.getDateUpdate();
        }

        @Override
        public MemberManagerItemDetail build() {
            return new MemberManagerItemDetail(this);
        }
    }
}

package com.jwi.groupwaremanager.model.member;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class MemberIsAdminRequest {
    @NotNull
    private Integer managerPin;
}

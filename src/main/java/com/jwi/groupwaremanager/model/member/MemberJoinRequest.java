package com.jwi.groupwaremanager.model.member;

import com.jwi.groupwaremanager.enums.member.Position;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@Setter
public class MemberJoinRequest {
    @NotNull
    @Length(min = 5, max = 20)
    @ApiModelProperty(value = "아이디", required = true)
    private String username;

    @NotNull
    @Length(min = 8, max = 20)
    @ApiModelProperty(value = "비밀번호", required = true)
    private String password;

    @NotNull
    @Length(min = 2, max = 20)
    @ApiModelProperty(value = "이름", required = true)
    private String name;

    @NotNull
    @ApiModelProperty(value = "생년월일", required = true)
    private LocalDate birth;

    @NotNull
    @Length(min = 13, max = 13)
    @ApiModelProperty(value = "연락처", required = true)
    private String phone;

    @NotNull
    @Length(min = 10, max = 50)
    @ApiModelProperty(value = "주소", required = true)
    private String address;

    @NotNull
    @Length(min = 3, max = 10)
    @ApiModelProperty(value = "부서", required = true)
    private String department;

    @NotNull
    @Enumerated(EnumType.STRING)
    @ApiModelProperty(value = "직급", required = true)
    private Position position;

    @NotNull
    @ApiModelProperty(value = "입사일", required = true)
    private LocalDate dateIn;

}

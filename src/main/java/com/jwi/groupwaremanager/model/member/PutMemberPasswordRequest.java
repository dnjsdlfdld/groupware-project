package com.jwi.groupwaremanager.model.member;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class PutMemberPasswordRequest {
    @NotNull
    @Length(min = 8, max = 20)
    @ApiModelProperty(value = "비밀번호", required = true)
    private String password;
}

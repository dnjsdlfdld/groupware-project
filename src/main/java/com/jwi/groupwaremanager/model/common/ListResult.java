package com.jwi.groupwaremanager.model.common;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class ListResult<T> extends CommonResult{ // <T> = 정해지지 않은 무언가
    private List<T> list;

    private Long totalItemCount;

    private Integer totalPage;

    private Integer currentPage;
}

package com.jwi.groupwaremanager.interfaces;

public interface CommonModelBuilder<T> {
    T build(); //
}

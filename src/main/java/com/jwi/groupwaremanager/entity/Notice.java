package com.jwi.groupwaremanager.entity;

import com.jwi.groupwaremanager.enums.notice.NoticeStatus;
import com.jwi.groupwaremanager.interfaces.CommonModelBuilder;
import com.jwi.groupwaremanager.model.notice.NoticeRequest;
import com.jwi.groupwaremanager.model.notice.PutNoticeDateRequest;
import com.jwi.groupwaremanager.model.notice.PutNoticeRequest;
import com.jwi.groupwaremanager.model.notice.PutNoticeStatus;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Notice {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 20)
    private String title;

    @Column(nullable = false, length = 200)
    private String content;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false, length = 14)
    private NoticeStatus noticeStatus;

    @Column(nullable = false, length = 20)
    private String writer;

    @Column(nullable = false)
    private LocalDate noticeStart;

    @Column(nullable = false)
    private LocalDate noticeEnd;

    @Column(nullable = false)
    private LocalDateTime dateCreate;

    @Column(nullable = false)
    private LocalDateTime dateUpdate;

    public void putNoticeStatus(PutNoticeStatus putNoticeStatus) {
        this.noticeStatus = putNoticeStatus.getNoticeStatus();
    }

    public void putNoticeDate(PutNoticeDateRequest request) {
        this.noticeStart = request.getNoticeStart();
        this.noticeEnd = request.getNoticeEnd();
    }

    public void putNotice(PutNoticeRequest request) {
        this.title = request.getTitle();
        this.content = request.getContent();
        this.writer = request.getWriter();
    }

    private Notice(NoticeBuilder builder) {
        this.title = builder.title;
        this.content = builder.content;
        this.noticeStatus = builder.noticeStatus;
        this.writer = builder.writer;
        this.noticeStart = builder.noticeStart;
        this.noticeEnd = builder.noticeEnd;
        this.dateCreate = builder.dateCreate;
        this.dateUpdate = builder.dateUpdate;
    }

    public static class NoticeBuilder implements CommonModelBuilder<Notice> {
        private final String title;
        private final String content;
        private final NoticeStatus noticeStatus;
        private final String writer;
        private final LocalDate noticeStart;
        private final LocalDate noticeEnd;
        private final LocalDateTime dateCreate;
        private final LocalDateTime dateUpdate;

        public NoticeBuilder(NoticeRequest request, NoticeStatus noticeStatus) {
            this.title = request.getTitle();
            this.content = request.getContent();
            this.noticeStatus = noticeStatus;
            this.writer = request.getWriter();
            this.noticeStart = request.getNoticeStart();
            this.noticeEnd = request.getNoticeEnd();
            this.dateCreate = LocalDateTime.now();
            this.dateUpdate = LocalDateTime.now();
        }

        @Override
        public Notice build() {
            return new Notice(this);
        }
    }
}

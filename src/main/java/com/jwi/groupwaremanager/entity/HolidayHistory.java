package com.jwi.groupwaremanager.entity;

import com.jwi.groupwaremanager.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class HolidayHistory {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "memberId", nullable = false)
    private Member member;

    @Column(nullable = false)
    @ApiModelProperty(notes = "true 차감, false 추가")
    private Boolean isGive;

    @ApiModelProperty(notes = "증감값")
    @Column(nullable = false)
    private Float increaseOrDecreaseValue;

    @Column(nullable = false)
    private LocalDateTime dateCreate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "uploadId")
    private HolidayUpload holidayUpload;

    private HolidayHistory(HolidayHistoryBuilder builder) {
        this.member = builder.member;
        this.isGive = builder.isGive;
        this.increaseOrDecreaseValue = builder.increaseOrDecreaseValue;
        this.dateCreate = builder.dateCreate;
    }

    public static class HolidayHistoryBuilder implements CommonModelBuilder<HolidayHistory> {

        private final Member member;
        private final Boolean isGive;
        private final Float increaseOrDecreaseValue;
        private final LocalDateTime dateCreate;

        public HolidayHistoryBuilder(Member member, Boolean isGive, Float increaseOrDecreaseValue) {
            this.member = member;
            this.isGive = isGive;
            this.increaseOrDecreaseValue = increaseOrDecreaseValue;
            this.dateCreate = LocalDateTime.now();
        }

        @Override
        public HolidayHistory build() {
            return new HolidayHistory(this);
        }
    }

    private HolidayHistory(HolidayHistoryUploadBuilder builder) {
        this.member = builder.member;
        this.isGive = builder.isGive;
        this.holidayUpload = builder.holidayUpload;
        this.increaseOrDecreaseValue = builder.holidayUpload.getUseHoliday();
        this.dateCreate = builder.dateCreate;
    }

    public static class HolidayHistoryUploadBuilder implements CommonModelBuilder<HolidayHistory> {

        private final Member member;
        private final Boolean isGive;
        private final HolidayUpload holidayUpload;
        private final LocalDateTime dateCreate;

        public HolidayHistoryUploadBuilder(Member member, HolidayUpload holidayUpload) {
            this.member = member;
            this.isGive = true;
            this.holidayUpload = holidayUpload;
            this.dateCreate = LocalDateTime.now();
        }

        @Override
        public HolidayHistory build() {
            return new HolidayHistory(this);
        }
    }
}

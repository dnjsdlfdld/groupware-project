package com.jwi.groupwaremanager.entity;

import com.jwi.groupwaremanager.enums.attendance.AttendanceType;
import com.jwi.groupwaremanager.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Attendance {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // 출근회원
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "memberId", nullable = false)
    private Member member;

    // 출근 기준일
    @Column(nullable = false)
    private LocalDate dateWork;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false, length = 15)
    private AttendanceType attendanceType;

    @Column(nullable = false)
    private LocalTime timeStart;

    private LocalTime timeEarlyLeave;

    private LocalTime timeLeave;

    public void putState(AttendanceType attendanceType) {
        this.attendanceType = attendanceType;

        switch (attendanceType) {
            case EARLY_LEAVE_WORK -> this.timeEarlyLeave = LocalTime.now();
            case LEAVE_WORK -> this.timeLeave = LocalTime.now();
        }
    }

    private Attendance(AttendanceTestBuilder builder) {
        this.member = builder.member;
        this.dateWork = builder.dateWork;
        this.attendanceType = builder.attendanceType;
        this.timeStart = builder.timeStart;
    }

    public static class AttendanceTestBuilder implements CommonModelBuilder<Attendance> {
        private final Member member;
        private final LocalDate dateWork;
        private final AttendanceType attendanceType;
        private final LocalTime timeStart;

        // 다른 것들은 조작이 되면 안 되기에 memberId만 받고 나머지는 javaTime 및 기본값 지정
        public AttendanceTestBuilder(Member member) {
            this.member = member;
            this.dateWork = LocalDate.now();
            this.attendanceType = AttendanceType.ATTENDANCE;
            this.timeStart = LocalTime.now();
        }

        @Override
        public Attendance build() {
            return new Attendance(this);
        }
    }
}

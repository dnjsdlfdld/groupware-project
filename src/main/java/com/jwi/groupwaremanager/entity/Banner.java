package com.jwi.groupwaremanager.entity;

import com.jwi.groupwaremanager.interfaces.CommonModelBuilder;
import com.jwi.groupwaremanager.model.banner.BannerRequest;
import com.jwi.groupwaremanager.model.banner.PutBannerRequest;
import com.jwi.groupwaremanager.model.banner.PutBannerDateRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Banner {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 150)
    private String imageUrl;

    @Column(nullable = false, length = 20)
    private String title;

    @Column(nullable = false)
    private LocalDate postStart;

    @Column(nullable = false)
    private LocalDate postEnd;

    @Column(nullable = false)
    private LocalDateTime dateCreate;

    @Column(nullable = false)
    private LocalDateTime dateUpdate;

    public void putBanner(PutBannerRequest request) {
        this.imageUrl = request.getImageUrl();
        this.title = request.getTitle();
    }

    public void putBannerDate(PutBannerDateRequest request) {
        this.postStart = request.getPostStart();
        this.postEnd = request.getPostEnd();
    }

    private Banner(BannerBuilder builder) {
        this.imageUrl = builder.imageUrl;
        this.title = builder.title;
        this.postStart = builder.postStart;
        this.postEnd = builder.postEnd;
        this.dateCreate = builder.dateCreate;
        this.dateUpdate = builder.dateUpdate;
    }

    public static class BannerBuilder implements CommonModelBuilder<Banner> {

        private final String imageUrl;
        private final String title;
        private final LocalDate postStart;
        private final LocalDate postEnd;
        private final LocalDateTime dateCreate;
        private final LocalDateTime dateUpdate;

        public BannerBuilder(BannerRequest request) {
            this.imageUrl = request.getImageUrl();
            this.title = request.getTitle();
            this.postStart = request.getPostStart();
            this.postEnd = request.getPostEnd();
            this.dateCreate = LocalDateTime.now();
            this.dateUpdate = LocalDateTime.now();
        }

        @Override
        public Banner build() {
            return new Banner(this);
        }
    }
}

package com.jwi.groupwaremanager.entity;

import com.jwi.groupwaremanager.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class HolidayCount {
    @ApiModelProperty(notes = "시퀀스")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long memberId;

    @ApiModelProperty(notes = "총 연차 보유 갯수")
    @Column(nullable = false)
    private Float countTotal;

    @ApiModelProperty(notes = "총 사용 갯수")
    @Column(nullable = false)
    private Float countUse;

    @ApiModelProperty(notes = "사용 가능 갯수")
    @Column(nullable = false)
    private Float haveHoliday;

    @ApiModelProperty(notes = "연차 유효 시작일")
    @Column(nullable = false)
    private LocalDate dateHolidayCountStart;

    @ApiModelProperty(notes = "연차 유효 종료일")
    @Column(nullable = false)
    private LocalDate dateHolidayCountEnd;

    @ApiModelProperty(notes = "등록일자")
    @Column(nullable = false)
    private LocalDateTime dateCreate;

    @ApiModelProperty(notes = "수정일자")
    @Column(nullable = false)
    private LocalDateTime dateUpdate;

    /**
     * num += 100 은
     * num = num + 100
     *
     * =+ 라는 연산자
     * num = (+100)
     */

    public void plusCountTotal(float plusCount) {
        this.countTotal += plusCount;
        this.haveHoliday += plusCount;
    }

    public void plusCountUse(float plusCount) {
        this.countUse += plusCount;
        this.haveHoliday -= plusCount;
    }

//    public void minusHaveHoliday() {
//        this.haveHoliday += (countTotal - countUse);
//    }

    private HolidayCount(HolidayCountBuilder builder) {
        this.memberId = builder.memberId;
        this.countTotal = builder.countTotal;
        this.countUse = builder.countUse;
        this.haveHoliday = builder.haveHoliday;
        this.dateHolidayCountStart = builder.dateHolidayCountStart;
        this.dateHolidayCountEnd = builder.dateHolidayCountEnd;
        this.dateCreate = builder.dateCreate;
        this.dateUpdate = builder.dateUpdate;
    }

    public static class HolidayCountBuilder implements CommonModelBuilder<HolidayCount> {

        private final Long memberId;
        private final Float countTotal;
        private final Float countUse;
        private final Float haveHoliday;
        private final LocalDate dateHolidayCountStart;
        private final LocalDate dateHolidayCountEnd;
        private final LocalDateTime dateCreate;
        private final LocalDateTime dateUpdate;

        public HolidayCountBuilder(Long memberId, LocalDate dateIn) {
            this.memberId = memberId;
            this.countTotal = 0f;
            this.countUse = 0f;
            this.haveHoliday = 0f;
            this.dateHolidayCountStart = dateIn;
            this.dateHolidayCountEnd = dateIn.plusYears(1).minusDays(1);
            this.dateCreate = LocalDateTime.now();
            this.dateUpdate = LocalDateTime.now();
        }

        @Override
        public HolidayCount build() {
            return new HolidayCount(this);
        }
    }
}

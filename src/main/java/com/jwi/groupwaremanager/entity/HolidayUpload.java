package com.jwi.groupwaremanager.entity;

import com.jwi.groupwaremanager.enums.holiday.ApprovalState;
import com.jwi.groupwaremanager.enums.holiday.HolidayType;
import com.jwi.groupwaremanager.interfaces.CommonModelBuilder;
import com.jwi.groupwaremanager.model.holiday.HolidayUserRequest;
import com.jwi.groupwaremanager.model.holiday.PutHolidayUploadRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class HolidayUpload {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "memberId", nullable = false)
    private Member member;

    @Column(nullable = false, length = 20) // 제목
    private String title;

    @Column(nullable = false, length = 50) // 내용
    private String content;

    @Enumerated(EnumType.STRING)  // 휴가 유형
    @Column(nullable = false, length = 18)
    private HolidayType holidayType;

    @Column(nullable = false) // 휴가 시작일자
    private LocalDateTime startHoliday;

    @Column(nullable = false) // 휴가 종료일자
    private LocalDateTime endHoliday;

    @Column(nullable = false) // 사용할 일 수
    private Float useHoliday;

    @Enumerated(EnumType.STRING)  // 결재 상태
    @Column(nullable = false)
    private ApprovalState approvalState;

    @ApiModelProperty(notes = "승인시간 > 결재완료 때만 작동")
    private LocalDateTime approvalDate;

    @ApiModelProperty(notes = "반려시간 > 반려처리 때만 작동")
    private LocalDateTime companionDate;

    @Column(nullable = false)
    private LocalDateTime dateCreate;

    @Column(nullable = false)
    private LocalDateTime dateUpdate;
    @ApiModelProperty(notes = "관리자 승인여부, true 면 승인, false 면 반려 ")
    private Boolean isEnable;

    public void putApprovalState() {
        this.approvalState = ApprovalState.COMPLETED_APPROVAL;
        this.approvalDate = LocalDateTime.now();
        this.dateUpdate = LocalDateTime.now();
        this.isEnable = true;
    }

    public void putCompanion() {
        this.approvalState = ApprovalState.RETURN_APPROVAL;
        this.companionDate = LocalDateTime.now();
        this.dateUpdate = LocalDateTime.now();
        this.isEnable = false;
    }

    public void putHolidayUpload(PutHolidayUploadRequest request) {
        this.title = request.getTitle();
        this.content = request.getContent();
        this.holidayType = request.getHolidayType();
        this.startHoliday = request.getStartHoliday();
        this.endHoliday = request.getEndHoliday();
        this.useHoliday = request.getUseHoliday();
        this.dateUpdate = LocalDateTime.now();
    }


    private HolidayUpload(HolidayUploadBuilder builder) {
        this.member = builder.member;
        this.title = builder.title;
        this.content = builder.content;
        this.holidayType = builder.holidayType;
        this.startHoliday = builder.startHoliday;
        this.endHoliday = builder.endHoliday;
        this.useHoliday = builder.useHoliday;
        this.approvalState = builder.approvalState;
        this.dateCreate = builder.dateCreate;
        this.dateUpdate = builder.dateUpdate;
    }

    public static class HolidayUploadBuilder implements CommonModelBuilder<HolidayUpload> {

        private final Member member;
        private final String title;
        private final String content;
        private final HolidayType holidayType;
        private final LocalDateTime startHoliday;
        private final LocalDateTime endHoliday;
        private final Float useHoliday;
        private final ApprovalState approvalState;
        private final LocalDateTime dateCreate;
        private final LocalDateTime dateUpdate;

        public HolidayUploadBuilder(Member member, HolidayUserRequest request) {
            this.member = member;
            this.title = request.getTitle();
            this.content = request.getContent();
            this.holidayType = request.getHolidayType();
            this.startHoliday = request.getStartHoliday();
            this.endHoliday = request.getEndHoliday();
            this.useHoliday = request.getUseHoliday();
            this.approvalState = ApprovalState.WAIT_APPROVAL;
            this.dateCreate = LocalDateTime.now();
            this.dateUpdate = LocalDateTime.now();

        }

        @Override
        public HolidayUpload build() {
            return new HolidayUpload(this);
        }
    }
}

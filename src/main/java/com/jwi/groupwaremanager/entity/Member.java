package com.jwi.groupwaremanager.entity;

import com.jwi.groupwaremanager.enums.member.Position;
import com.jwi.groupwaremanager.interfaces.CommonModelBuilder;
import com.jwi.groupwaremanager.model.member.*;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Member {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // 아이디가 중복되지 않게끔 처리
    @Column(nullable = false, length = 20, unique = true)
    private String username;

    @Column(nullable = false, length = 20)
    private String password;

    @Column(nullable = false, length = 20)
    private String name;

    @Column(nullable = false)
    private LocalDate birth;

    @Column(nullable = false, length = 20)
    private String phone;

    @Column(nullable = false, length = 50)
    private String address;

    // 부서
    @Column(nullable = false, length = 10)
    private String department;

    // 직급
    @Enumerated(EnumType.STRING)
    @Column(nullable = false, length = 15)
    private Position position;

    @Column(nullable = false)
    private Boolean isAdmin;

    // 입사일
    @Column(nullable = false)
    private LocalDate dateIn;

    // 퇴사여부
    @Column(nullable = false)
    private Boolean isEnable;

    // 퇴사일자 null값 허용
    private LocalDate dateOut;

    @Column(nullable = false)
    private LocalDateTime dateCreate;

    @Column(nullable = false)
    private LocalDateTime dateUpdate;

    private LocalDateTime loginTime;

    private Integer managerPin;

    public void putMemberCompanyInfo(PutMemberCompanyInfoRequest request) {
        this.department = request.getDepartment();
        this.position = request.getPosition();
    }

    public void putLoginTime() {
        this.loginTime = LocalDateTime.now();
    }

    public void putPosition(PutMemberPositionRequest request) {
        this. position = request.position;
    }

    public void putUserInfo(PutMemberUserRequest request) {
        this.phone = request.getPhone();
        this.address = request.getAddress();
    }

    // 권한(직원용) 수정
    public void putIsAdmin() {
        this.isAdmin = false;
        this.dateUpdate = LocalDateTime.now();
    }

    // 권한(관리자) 수정
    public void putIsAdminManager(MemberIsAdminRequest request) {
        this.isAdmin = true;
        this.managerPin = request.getManagerPin();
        this.dateUpdate = LocalDateTime.now();
    }

    // 퇴사처리
    public void putIsEnable() {
        this.isEnable = false;
        this.dateOut = LocalDate.now();
    }

    // 비밀번호 수정
    public void putPassword(PutMemberPasswordRequest request) {
        this.password = request.getPassword();
        this.dateUpdate = LocalDateTime.now();
    }

    // 직원 정보 수정
    public void putMember(PutMemberRequest request) {
        this.name = request.getName();
        this.phone = request.getPhone();
        this.address = request.getAddress();
//        this.department = request.getDepartment();
//        this.position = request.getPosition();
        this.dateUpdate = LocalDateTime.now();
    }

    private Member(MemberBuilder builder) {
        this.username = builder.username;
        this.password = builder.password;
        this.name = builder.name;
        this.birth = builder.birth;
        this.phone = builder.phone;
        this.address = builder.address;
        this.department = builder.department;
        this.position = builder.position;
        this.isAdmin = builder.isAdmin;
        this.dateIn = builder.dateIn;
        this.isEnable = builder.isEnable;
        this.dateCreate = builder.dateCreate;
        this.dateUpdate = builder.dateUpdate;
    }

    public static class MemberBuilder implements CommonModelBuilder<Member> {

        private final String username;
        private final String password;
        private final String name;
        private final LocalDate birth;
        private final String phone;
        private final String address;
        private final String department;
        private final Position position;
        private final Boolean isAdmin;
        private final LocalDate dateIn;
        private final Boolean isEnable;
        private final LocalDateTime dateCreate;
        private final LocalDateTime dateUpdate;

        public MemberBuilder(MemberJoinRequest request) {
            this.username = request.getUsername();
            this.password = request.getPassword();
            this.name = request.getName();
            this.birth = request.getBirth();
            this.phone = request.getPhone();
            this.address = request.getAddress();
            this.department = request.getDepartment();
            this.position = request.getPosition();
            this.isAdmin = false;  // 권한 여부 false 는 일반 true는 관리자
            this.dateIn = request.getDateIn();
            this.isEnable = true;
            this.dateCreate = LocalDateTime.now();
            this.dateUpdate = LocalDateTime.now();
        }

        @Override
        public Member build() {
            return new Member(this);
        }
    }
}
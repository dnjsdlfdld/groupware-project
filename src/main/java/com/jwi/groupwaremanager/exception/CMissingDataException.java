package com.jwi.groupwaremanager.exception;
// Custom
public class CMissingDataException extends RuntimeException{
    public CMissingDataException(String msg, Throwable t) { // 생성자 3개
        super(msg, t);
    }

    public CMissingDataException(String msg) {
        super(msg);
    }

    public CMissingDataException() {
        super();
    }

}

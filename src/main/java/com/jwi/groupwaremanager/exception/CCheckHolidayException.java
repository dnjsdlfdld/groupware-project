package com.jwi.groupwaremanager.exception;
// Custom
public class CCheckHolidayException extends RuntimeException{
    public CCheckHolidayException(String msg, Throwable t) { // 생성자 3개
        super(msg, t);
    }

    public CCheckHolidayException(String msg) {
        super(msg);
    }

    public CCheckHolidayException() {
        super();
    }

}

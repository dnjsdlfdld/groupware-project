package com.jwi.groupwaremanager.exception;
// Custom
public class CNoInformationException extends RuntimeException{
    public CNoInformationException(String msg, Throwable t) { // 생성자 3개
        super(msg, t);
    }

    public CNoInformationException(String msg) {
        super(msg);
    }

    public CNoInformationException() {
        super();
    }

}

package com.jwi.groupwaremanager.exception;
// Custom
public class CNoCheckWorkException extends RuntimeException{
    public CNoCheckWorkException(String msg, Throwable t) { // 생성자 3개
        super(msg, t);
    }

    public CNoCheckWorkException(String msg) {
        super(msg);
    }

    public CNoCheckWorkException() {
        super();
    }

}

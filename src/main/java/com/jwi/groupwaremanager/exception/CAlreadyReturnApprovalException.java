package com.jwi.groupwaremanager.exception;
// Custom
public class CAlreadyReturnApprovalException extends RuntimeException{
    public CAlreadyReturnApprovalException(String msg, Throwable t) { // 생성자 3개
        super(msg, t);
    }

    public CAlreadyReturnApprovalException(String msg) {
        super(msg);
    }

    public CAlreadyReturnApprovalException() {
        super();
    }

}

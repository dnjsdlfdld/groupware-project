package com.jwi.groupwaremanager.exception;
// Custom
public class CDoNotPasswordException extends RuntimeException{
    public CDoNotPasswordException(String msg, Throwable t) { // 생성자 3개
        super(msg, t);
    }

    public CDoNotPasswordException(String msg) {
        super(msg);
    }

    public CDoNotPasswordException() {
        super();
    }

}

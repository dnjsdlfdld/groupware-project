package com.jwi.groupwaremanager.exception;
// Custom
public class CNoSameDataException extends RuntimeException{
    public CNoSameDataException(String msg, Throwable t) { // 생성자 3개
        super(msg, t);
    }

    public CNoSameDataException(String msg) {
        super(msg);
    }

    public CNoSameDataException() {
        super();
    }

}

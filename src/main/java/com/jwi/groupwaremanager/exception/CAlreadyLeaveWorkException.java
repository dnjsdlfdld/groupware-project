package com.jwi.groupwaremanager.exception;
// Custom
public class CAlreadyLeaveWorkException extends RuntimeException{
    public CAlreadyLeaveWorkException(String msg, Throwable t) { // 생성자 3개
        super(msg, t);
    }

    public CAlreadyLeaveWorkException(String msg) {
        super(msg);
    }

    public CAlreadyLeaveWorkException() {
        super();
    }

}

package com.jwi.groupwaremanager.exception;
// Custom
public class CAlreadyEarlyLeaveWorkException extends RuntimeException{
    public CAlreadyEarlyLeaveWorkException(String msg, Throwable t) { // 생성자 3개
        super(msg, t);
    }

    public CAlreadyEarlyLeaveWorkException(String msg) {
        super(msg);
    }

    public CAlreadyEarlyLeaveWorkException() {
        super();
    }

}

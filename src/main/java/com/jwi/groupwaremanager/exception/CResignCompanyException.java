package com.jwi.groupwaremanager.exception;
// Custom
public class CResignCompanyException extends RuntimeException{
    public CResignCompanyException(String msg, Throwable t) { // 생성자 3개
        super(msg, t);
    }

    public CResignCompanyException(String msg) {
        super(msg);
    }

    public CResignCompanyException() {
        super();
    }

}

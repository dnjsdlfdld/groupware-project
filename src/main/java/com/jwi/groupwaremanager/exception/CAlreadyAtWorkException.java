package com.jwi.groupwaremanager.exception;
// Custom
public class CAlreadyAtWorkException extends RuntimeException{
    public CAlreadyAtWorkException(String msg, Throwable t) { // 생성자 3개
        super(msg, t);
    }

    public CAlreadyAtWorkException(String msg) {
        super(msg);
    }

    public CAlreadyAtWorkException() {
        super();
    }

}

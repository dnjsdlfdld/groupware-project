package com.jwi.groupwaremanager.exception;
// Custom
public class CNoDataException extends RuntimeException{
    public CNoDataException(String msg, Throwable t) { // 생성자 3개
        super(msg, t);
    }

    public CNoDataException(String msg) {
        super(msg);
    }

    public CNoDataException() {
        super();
    }

}

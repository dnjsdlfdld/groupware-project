package com.jwi.groupwaremanager.exception;
// Custom
public class CNoAttendanceRecordException extends RuntimeException{
    public CNoAttendanceRecordException(String msg, Throwable t) { // 생성자 3개
        super(msg, t);
    }

    public CNoAttendanceRecordException(String msg) {
        super(msg);
    }

    public CNoAttendanceRecordException() {
        super();
    }

}

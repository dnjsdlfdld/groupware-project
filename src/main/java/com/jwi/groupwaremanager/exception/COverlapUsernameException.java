package com.jwi.groupwaremanager.exception;
// Custom
public class COverlapUsernameException extends RuntimeException{
    public COverlapUsernameException(String msg, Throwable t) { // 생성자 3개
        super(msg, t);
    }

    public COverlapUsernameException(String msg) {
        super(msg);
    }

    public COverlapUsernameException() {
        super();
    }

}

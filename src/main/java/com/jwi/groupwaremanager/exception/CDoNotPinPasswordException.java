package com.jwi.groupwaremanager.exception;
// Custom
public class CDoNotPinPasswordException extends RuntimeException{
    public CDoNotPinPasswordException(String msg, Throwable t) { // 생성자 3개
        super(msg, t);
    }

    public CDoNotPinPasswordException(String msg) {
        super(msg);
    }

    public CDoNotPinPasswordException() {
        super();
    }

}

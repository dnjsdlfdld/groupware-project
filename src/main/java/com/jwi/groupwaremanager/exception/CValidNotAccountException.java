package com.jwi.groupwaremanager.exception;
// Custom
public class CValidNotAccountException extends RuntimeException{
    public CValidNotAccountException(String msg, Throwable t) { // 생성자 3개
        super(msg, t);
    }

    public CValidNotAccountException(String msg) {
        super(msg);
    }

    public CValidNotAccountException() {
        super();
    }

}

package com.jwi.groupwaremanager.controller;

import com.jwi.groupwaremanager.model.common.SingleResult;
import com.jwi.groupwaremanager.model.login.ManagerLoginRequest;
import com.jwi.groupwaremanager.model.login.MemberLoginRequest;
import com.jwi.groupwaremanager.model.login.LoginResponse;
import com.jwi.groupwaremanager.service.LoginService;
import com.jwi.groupwaremanager.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "로그인")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/login")
public class LoginController {
    private final LoginService loginService;

    @ApiOperation(value = "관리자 로그인")
    @PostMapping("/manager")
    public SingleResult<LoginResponse> doLoginManager(@RequestBody @Valid ManagerLoginRequest request) {
        return ResponseService.getSingleResult(loginService.doLoginManager(request));
    }

    @ApiOperation(value = "직원 로그인")
    @PostMapping("/user")
    public SingleResult<LoginResponse> doLoginUser(@RequestBody @Valid MemberLoginRequest request) {
        return ResponseService.getSingleResult(loginService.doLoginUser(request));
    }
    @ApiOperation(value = "로그인 정보값 받아오기")
    @GetMapping("/login-info/{memberId}")
    public SingleResult<LoginResponse> getLoginInfo(@PathVariable long memberId) {
        return ResponseService.getSingleResult(loginService.getLoginInfo(memberId));
    }
}

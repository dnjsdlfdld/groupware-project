package com.jwi.groupwaremanager.controller;

import com.jwi.groupwaremanager.model.banner.*;
import com.jwi.groupwaremanager.model.common.CommonResult;
import com.jwi.groupwaremanager.model.common.ListResult;
import com.jwi.groupwaremanager.service.BannerService;
import com.jwi.groupwaremanager.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "배너관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/banner")
public class BannerController {
    private final BannerService bannerService;

    @ApiOperation(value = "배너 등록")
    @PostMapping("/new")
    public CommonResult setBanner(@RequestBody @Valid BannerRequest request) {
        bannerService.setBanner(request);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "홈화면용 배너")
    @GetMapping("/banner-home")
    public ListResult<BannerHomeItem> getBannerHome() {
        return ResponseService.getListResult(bannerService.getBannerHome(), true);
    }

    @ApiOperation(value = "배너 리스트")
    @GetMapping("/banner-detail")
    public ListResult<BannerDetailItem> getBannerDetail() {
        return ResponseService.getListResult(bannerService.getBannerDetail(), true);
    }

    @ApiImplicitParams({
            @ApiImplicitParam(name = "bannerId", value = "배너 구분번호", required = true)
    })
    @ApiOperation(value = "배너 정보수정")
    @PutMapping("/put-banner/{bannerId}")
    public CommonResult putBanner(@PathVariable long bannerId, @RequestBody @Valid PutBannerRequest request) {
        bannerService.putBanner(bannerId, request);
        return ResponseService.getSuccessResult();
    }

    @ApiImplicitParams({
            @ApiImplicitParam(name = "bannerId", value = "배너 구분번호", required = true)
    })
    @ApiOperation(value = "게시일자 수정")
    @PutMapping("/put-banner-detail/{bannerId}")
    public CommonResult putBannerDate(@PathVariable long bannerId, @RequestBody @Valid PutBannerDateRequest request) {
        bannerService.putBannerDate(bannerId, request);
        return ResponseService.getSuccessResult();
    }

    @ApiImplicitParams({
            @ApiImplicitParam(name = "bannerId", value = "배너 구분번호", required = true)
    })
    @ApiOperation(value = "배너 삭제")
    @DeleteMapping("/del/{bannerId}")
    public CommonResult delBanner(@PathVariable long bannerId) {
        bannerService.delBanner(bannerId);
        return ResponseService.getSuccessResult();
    }
}

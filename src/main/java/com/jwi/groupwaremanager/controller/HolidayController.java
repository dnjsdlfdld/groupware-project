package com.jwi.groupwaremanager.controller;

import com.jwi.groupwaremanager.entity.HolidayUpload;
import com.jwi.groupwaremanager.entity.Member;
import com.jwi.groupwaremanager.model.common.CommonResult;
import com.jwi.groupwaremanager.model.common.ListResult;
import com.jwi.groupwaremanager.model.common.SingleResult;
import com.jwi.groupwaremanager.model.holiday.HolidaySimpleList;
import com.jwi.groupwaremanager.model.holiday.HolidayUploadItem;
import com.jwi.groupwaremanager.model.holiday.HolidayUserRequest;
import com.jwi.groupwaremanager.service.HolidayService;
import com.jwi.groupwaremanager.service.MemberService;
import com.jwi.groupwaremanager.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;

@Api(tags = "휴가 대장")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/holiday")
public class HolidayController {
    private final HolidayService holidayService;
    private final MemberService memberService;

    @ApiImplicitParams({
            @ApiImplicitParam(name = "memberId", value = "직원 등록번호", required = true)
    })
    @ApiOperation(value = "사원 등록 후 연차 초기값 세팅")
    @PostMapping("/set-holiday/{memberId}")
    public CommonResult setHoliday(
            @PathVariable long memberId,
            @RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd")LocalDate dateIn
    ) {
        Member member = memberService.getMemberId(memberId);
        holidayService.setCount(member.getId(), dateIn);
        return ResponseService.getSuccessResult();
    }

    @ApiImplicitParams({
            @ApiImplicitParam(name = "uploadId", value = "사원 등록번호", required = true)
    })
    @ApiOperation(value = "휴가 디테일 조회")
    @GetMapping("/detail/{uploadId}")
    public SingleResult<HolidayUploadItem> getUserUploads(@PathVariable long uploadId) {
        return ResponseService.getSingleResult(holidayService.getUserUploads(uploadId));
    }

    @ApiOperation(value = "전직원 휴가리스트 조회")
    @GetMapping("/list-simple")
    public ListResult<HolidaySimpleList> getHolidaySimpleList() {
        return ResponseService.getListResult(holidayService.getUploadSimpleList(), true);
    }

    @ApiOperation(value = "관리자가 날짜 검색으로 휴가 리스트 조회할 때")
    @GetMapping("/get-date-upload")
    public ListResult<HolidayUploadItem> getDateUserUpload(
            @RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate dateStart,
            @RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate dateEnd
            ) {
        return ResponseService.getListResult(holidayService.getDateUserUpload(dateStart, dateEnd), true);

    }

    @ApiImplicitParams({
            @ApiImplicitParam(name = "memberId", value = "직원 등록번호", required = true)
    })
    @ApiOperation(value = "연차 지급 및 차감")
    @PutMapping("/put-holiday/{memberId}")
    public CommonResult putHolidayCount(@PathVariable long memberId, boolean isGive, float increaseOrDecreaseValue) {
        Member member = memberService.getMemberId(memberId);
        holidayService.putHolidayCount(member, isGive, increaseOrDecreaseValue);

        return ResponseService.getSuccessResult();
    }

    @ApiImplicitParams({
            @ApiImplicitParam(name = "uploadId", value = "휴가 등록번호", required = true)
    })
    @ApiOperation(value = "휴가 결재")
    @PutMapping("upload/{uploadId}")
    public CommonResult putHolidayState(@PathVariable long uploadId, @RequestParam boolean isEnable) {
        HolidayUpload holidayUpload = holidayService.getUploadId(uploadId);
        holidayService.putHolidayState(holidayUpload, isEnable);

        return ResponseService.getSuccessResult();
    }

    // ----------사용자

    @ApiImplicitParams({
            @ApiImplicitParam(name = "memberId", value = "직원 등록번호", required = true)
    })
    @ApiOperation(value = "직원 휴가 신청서")
    @PostMapping("/user-holiday/{memberId}")
    public CommonResult setHolidayUser(@PathVariable long memberId, @RequestBody @Valid HolidayUserRequest request) {
        Member member = memberService.getMemberId(memberId);
        holidayService.setHolidayUser(member, request);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "사용자가 년도별 연차결재 조회")
    @GetMapping("/year/{memberId}")
    public ListResult<HolidayUploadItem> getYearUploads(
            @PathVariable long memberId,
            int selectYear) {
        return ResponseService.getListResult(holidayService.getYearUploads(memberId, selectYear), true);
    }

    @ApiImplicitParams({
            @ApiImplicitParam(name = "memberId", value = "직원 등록번호", required = true)
    })
    @ApiOperation(value = "직원이 자신이 올린 휴가를 볼 때")
    @GetMapping("/get-upload/{memberId}")
    public SingleResult<HolidayUploadItem> getUserUpload(@PathVariable long memberId) {
        return ResponseService.getSingleResult(holidayService.getUserUpload(memberId));
    }
}

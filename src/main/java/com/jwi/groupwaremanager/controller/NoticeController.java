package com.jwi.groupwaremanager.controller;

import com.jwi.groupwaremanager.enums.notice.NoticeStatus;
import com.jwi.groupwaremanager.model.common.CommonResult;
import com.jwi.groupwaremanager.model.common.ListResult;
import com.jwi.groupwaremanager.model.notice.*;
import com.jwi.groupwaremanager.service.NoticeService;
import com.jwi.groupwaremanager.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "공지사항")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/notice")
public class NoticeController {
    private final NoticeService noticeService;

    @ApiOperation(value = "공지사항 등록")
    @PostMapping("/new-notice")
    public CommonResult setNotice(NoticeStatus noticeStatus, @RequestBody @Valid NoticeRequest request) {
        noticeService.setNotice(noticeStatus, request);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "홈화면용 GET")
    @GetMapping("/top")
    public ListResult<NoticeHomeItem> getTopNotice() {
        return ResponseService.getListResult(noticeService.getTopNotice(), true);
    }

    public ListResult<NoticeDetailItem> getNoticeDetail() {
        return ResponseService.getListResult(noticeService.getNoticeDetail(), true);
    }

//    @ApiImplicitParams({
//            @ApiImplicitParam(name = "noticeId", value = "공지사항 구분번호", required = true)
//    })
//    @ApiOperation(value = "공지사항 수정")
//    @PutMapping("/notice/{noticeId}")
//    public CommonResult putNotice(@PathVariable long noticeId, @RequestBody @Valid PutNoticeRequest request) {
//        noticeService.putNotice(noticeId, request);
//        return ResponseService.getSuccessResult();
//    }

//    @ApiImplicitParams({
//            @ApiImplicitParam(name = "noticeId", value = "공지사항 구분번호", required = true)
//    })
//    @ApiOperation(value = "게시일자 수정")
//    @PutMapping("/notice-date/{noticeId}")
//    public CommonResult putNoticeDate(@PathVariable long noticeId, @RequestBody @Valid PutNoticeDateRequest request) {
//        noticeService.putNoticeDate(noticeId, request);
//        return ResponseService.getSuccessResult();
//    }

//    @ApiImplicitParams({
//            @ApiImplicitParam(name = "noticeId", value = "공지사항 구분번호", required = true)
//    })
//    @ApiOperation(value = "공개여부 수정")
//    @PutMapping("/notice-status/{noticeId}")
//    public CommonResult putNoticeStatus(@PathVariable long noticeId, @RequestBody @Valid PutNoticeStatus putNoticeStatus) {
//        noticeService.putNoticeStatus(noticeId, putNoticeStatus);
//        return ResponseService.getSuccessResult();
//    }

//    @ApiImplicitParams({
//            @ApiImplicitParam(name = "noticeId", value = "공지사항 구분번호", required = true)
//    })
//    @ApiOperation(value = "공지사항 삭제")
//    @DeleteMapping("/del/{noticeId}")
//    public CommonResult delNotice(@PathVariable long noticeId) {
//        noticeService.delNotice(noticeId);
//        return ResponseService.getSuccessResult();
//    }
}

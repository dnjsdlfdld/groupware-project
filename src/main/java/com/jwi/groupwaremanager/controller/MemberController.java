package com.jwi.groupwaremanager.controller;

import com.jwi.groupwaremanager.entity.Member;
import com.jwi.groupwaremanager.enums.member.Position;
import com.jwi.groupwaremanager.model.common.CommonResult;
import com.jwi.groupwaremanager.model.common.ListResult;
import com.jwi.groupwaremanager.model.common.SingleResult;
import com.jwi.groupwaremanager.model.member.*;
import com.jwi.groupwaremanager.service.HolidayService;
import com.jwi.groupwaremanager.service.MemberService;
import com.jwi.groupwaremanager.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "직원 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/member")
public class MemberController {
    private final MemberService memberService;
    private final HolidayService holidayService;

    @ApiOperation(value = "직원 등록")
    @PostMapping("/new-join")
    public CommonResult setMemberJoin(@RequestBody @Valid MemberJoinRequest request) {
        Member member = memberService.setMemberJoin(request);
        holidayService.setCount(member.getId(), member.getDateIn());
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "부서별 조회")
    @GetMapping("/search-department")
    public ListResult<MemberManagerItemDetail> getMemberDepartment(String department) {
        return ResponseService.getListResult(memberService.getMemberDepartment(department), true);
    }

    @ApiOperation(value = "직급별 조회")
    @GetMapping("/search-position")
    public ListResult<MemberManagerItemDetail> getMemberPosition(Position position) {
        return ResponseService.getListResult(memberService.getMemberPosition(position), true);
    }

    @ApiOperation(value = "이름 조회")
    @GetMapping("/search-name")
    public ListResult<MemberManagerItemDetail> getMemberName(String name) {
        return ResponseService.getListResult(memberService.getMembersName(name), true);
    }

    //@ApiImplicitParams({
    //        @ApiImplicitParam(name = "memberId", value = "직원 등록번호", required = true)
    //})
    //@ApiOperation(value = "아이디 조회")
   // @GetMapping("/search-id/{memberId}")
    //public SingleResult<MemberManagerItemDetail> getMember(@PathVariable long memberId) {
   //     return ResponseService.getSingleResult(memberService.getMember(memberId));
   // }


    @ApiImplicitParams({
            @ApiImplicitParam(name = "memberId", value = "직원 등록번호", required = true)
    })
    @ApiOperation(value = "단수 직원 리스트 조회")
    @GetMapping("/detail/{memberId}")
    public SingleResult<MemberManagerItemDetail> getMembersDetail(@PathVariable long memberId) {
        return ResponseService.getSingleResult(memberService.getMemberDetail(memberId));
    }

    @ApiImplicitParams({
            @ApiImplicitParam(name = "memberId", value = "직원 등록번호", required = true)
    })
    @ApiOperation(value = "내 정보 조회")
    @GetMapping("/search-id-user/{memberId}")
    public SingleResult<MemberUserItem> getMyInfo(@PathVariable long memberId) {
        return ResponseService.getSingleResult(memberService.getMyInfo(memberId));
    }

    @ApiImplicitParams({
            @ApiImplicitParam(name = "memberId", value = "직원 등록번호", required = true)
    })
    @ApiOperation(value = "비밀번호 변경")
    @PutMapping("/put-password/{memberId}")
    public CommonResult putPassword(@PathVariable long memberId, PutMemberPasswordRequest request) {
        memberService.putPassword(memberId, request);
        return ResponseService.getSuccessResult();
    }

    @ApiImplicitParams({
            @ApiImplicitParam(name = "memberId", value = "직원 등록번호", required = true)
    })
    @ApiOperation(value = "관리자로 권한 변경")
    @PutMapping("/put-admin-true/{memberId}")
    public CommonResult putIsAdminManager(@PathVariable long memberId, @RequestBody @Valid MemberIsAdminRequest request) {
        memberService.putIsAdminManager(memberId, request);
        return ResponseService.getSuccessResult();
    }

    @ApiImplicitParams({
            @ApiImplicitParam(name = "memberId", value = "직원 등록번호", required = true)
    })
    @ApiOperation(value = "임직원으로 권한 변경")
    @PutMapping("/put-admin-false/{memberId}")
    public CommonResult putIsAdmin(@PathVariable long memberId) {
        memberService.putIsAdmin(memberId);
        return ResponseService.getSuccessResult();
    }

    @ApiImplicitParams({
            @ApiImplicitParam(name = "memberId", value = "직원 등록번호", required = true)
    })
    @ApiOperation(value = "퇴사처리")
    @DeleteMapping("/del-id/{memberId}")
    public CommonResult delMemberIsEnable(@PathVariable long memberId) {
        memberService.putIsEnable(memberId);
        return ResponseService.getSuccessResult();
    }

    @ApiImplicitParams({
            @ApiImplicitParam(name = "memberId", value = "직원 등록번호", required = true)
    })
    @ApiOperation(value = "정보 수정(user)")
    @PutMapping("/put-user/{memberId}")
    public CommonResult putUserInfo(@PathVariable long memberId, @RequestBody @Valid PutMemberUserRequest request) {
        memberService.putUserInfo(memberId, request);
        return ResponseService.getSuccessResult();
    }

    // 직급 수정 (관리자용)

    @ApiImplicitParams({
            @ApiImplicitParam(name = "memberId", value = "직원 등록번호", required = true)
    })
    @ApiOperation(value = "직급 수정 (관리자용)")
    @PutMapping("/put-position/{memberId}")
    public CommonResult putMemberPosition(@PathVariable long memberId, @RequestBody @Valid PutMemberPositionRequest request) {
        memberService.putMemberPosition(memberId, request);
        return ResponseService.getSuccessResult();
    }

    @ApiImplicitParams({
            @ApiImplicitParam(name = "memberId", value = "직원 등록번호", required = true)
    })
    @ApiOperation(value = "사원 개인정보 수정 (관리자용)")
    @PutMapping("/personal-info/{memberId}")
    public CommonResult putMemberPersonal(@PathVariable long memberId, @RequestBody @Valid PutMemberRequest request) {
        memberService.putMember(memberId, request);
        return ResponseService.getSuccessResult();
    }
    @ApiImplicitParams({
            @ApiImplicitParam(name = "memberId", value = "직원 등록번호", required = true)
    })
    @ApiOperation(value = "사원 인사정보 수정 (관리자용)")
    @PutMapping("/company-info/{memberId}")
    public CommonResult putMemberCompany(@PathVariable long memberId, @RequestBody @Valid PutMemberCompanyInfoRequest request) {
        memberService.putMemberCompanyInfo(memberId, request);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "사원 일부정보 리스트")
    @GetMapping("/members/all")
    public ListResult<MemberManageItem> getMembers() {
        return ResponseService.getListResult(memberService.getMembers(), true);
    }
}

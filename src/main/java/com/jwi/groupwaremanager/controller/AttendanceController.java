package com.jwi.groupwaremanager.controller;

import com.jwi.groupwaremanager.entity.Member;
import com.jwi.groupwaremanager.enums.attendance.AttendanceType;
import com.jwi.groupwaremanager.model.attendance.AttendanceResponse;
import com.jwi.groupwaremanager.model.attendance.AttendanceDetailItem;
import com.jwi.groupwaremanager.model.attendance.AttendanceListItem;
import com.jwi.groupwaremanager.model.common.ListResult;
import com.jwi.groupwaremanager.model.common.SingleResult;
import com.jwi.groupwaremanager.service.AttendanceService;
import com.jwi.groupwaremanager.service.MemberService;
import com.jwi.groupwaremanager.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@Api(tags = "출결 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/attendance")
public class AttendanceController {
    private final AttendanceService attendanceService;

    private final MemberService memberService;

    @ApiOperation(value = "출근 일부데이터 끌어오기")
    @GetMapping("/attendance-list")
    public ListResult<AttendanceListItem> getAttendanceList() {
        return ResponseService.getListResult(attendanceService.getAttendanceList(), true);
    }

    @ApiOperation(value = "근태 상태 가져오기")
    @GetMapping("/type/member-id/{memberId}")
    public SingleResult<AttendanceResponse> getStatus(@PathVariable long memberId) {
        Member member = memberService.getMemberId(memberId);
        return ResponseService.getSingleResult(attendanceService.getCurrentStatus(memberId));
    }


    @ApiOperation(value = "근태 상태 수정")
    @PutMapping("/type/{attendanceType}/member-id/{memberId}")
    public SingleResult<AttendanceResponse> doStatusChange(
            @PathVariable AttendanceType attendanceType,
            @PathVariable long memberId
    ) {
        Member member = memberService.getMemberId(memberId);
        return ResponseService.getSingleResult(attendanceService.doAttendanceChange(member, attendanceType));
    }

    @ApiImplicitParams({
            @ApiImplicitParam(name = "memberId", value = "사원 등록번호", required = true)
    })
    @ApiOperation(value = "직원 출근상세")
    @GetMapping("/detail/{memberId}")
    public SingleResult<AttendanceDetailItem> getAttendanceDetail(@PathVariable long memberId) {
        return ResponseService.getSingleResult(attendanceService.getAttendanceDetail(memberId));
    }

    @ApiImplicitParams({
            @ApiImplicitParam(name = "memberId", value = "직원 등록번호", required = true)
    })
    @ApiOperation(value = "이달의 근무일수 확인(년, 월 안 줌)")
    @GetMapping("/month-work-day-copy/{memberId}")
    public long getMonthWorkDay(@PathVariable long memberId) {
        Member member = memberService.getMemberId(memberId);
        return attendanceService.getMonthWorkCopy(member);
    }
}

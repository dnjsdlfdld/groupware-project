package com.jwi.groupwaremanager.enums.holiday;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum HolidayType {
    ANNUAL("연차"),
    MORNING_HALF_DAY("오전반차"),
    AFTERNOON_HALF_DAY("오후반차"),
    SICK_LEAVE("병가"),
    LEAVE_OF_ABSENCE("공가");

    private final String name;
}

package com.jwi.groupwaremanager.enums.holiday;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ApprovalState {
    WAIT_APPROVAL("결재 대기중"),
    COMPLETED_APPROVAL("결재완료"),
    RETURN_APPROVAL("결재 반려");

    private final String name;
}

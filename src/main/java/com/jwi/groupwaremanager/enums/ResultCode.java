package com.jwi.groupwaremanager.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ResultCode {
    SUCCESS(0, "성공하였습니다."),
    FAILED(-1, "실패하였습니다."),
    MISSING_DATA(-10000, "데이터를 찾을 수 없습니다."),
    NO_DATA(-20000, "이미 출근 상태입니다."),
    NO_INFORMATION(-11111, "퇴사했거나 등록되지 않은 사원입니다."),
    ALREADY_AT_WORK(-10001, "이미 출근 상태입니다."),
    NO_CHECK_WORK(-10002, "출근 기록이 없습니다."),
    ALREADY_LEAVE_WORK(-10003, "이미 퇴근 처리되었습니다."),
    NO_SAME_DATA(-10004, "같은 상태로 변경할 수 없습니다."),

    ALREADY_EARLY_LEAVE_WORK(-10005, "이미 조퇴 처리 되었습니다."),
    DO_NOT_PASSWORD(-30000, "비밀번호가 맞지 않습니다."),
    VALID_NOT_ACCOUNT(-30001, "계정이 유효하지 않습니다."),

    RESIGN_COMPANY(-30002, "퇴사한 사원입니다. 관리자에게 문의바랍니다."),
    CHECK_HOLIDAY(-40000, "보유 연차개수를 확인해주세요."),
    ALREADY_RETURN_APPROVAL(-40001, "이미 반려 처리된 결재입니다."),
    NO_ATTENDANCE_RECORD(-40002, "금일 근태기록이 없습니다."),

    OVERLAP_USERNAME(-10, "중복되는 아이디입니다."),

    DO_NOT_PIN_PASSWORD(-11, "PIN번호가 일치하지 않습니다.")
    ;

    private final Integer code;
    private final String msg;
}
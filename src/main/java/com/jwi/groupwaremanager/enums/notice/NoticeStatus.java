package com.jwi.groupwaremanager.enums.notice;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum NoticeStatus {
    FULL_PUBLIC("전체공개"),
    NONDISCLOSURE("비공개"),
    MANAGER_PUBLIC("관리자만 공개");

    private final String name;
}

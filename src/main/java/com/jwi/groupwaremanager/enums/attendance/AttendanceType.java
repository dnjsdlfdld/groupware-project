package com.jwi.groupwaremanager.enums.attendance;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum AttendanceType {
    NONE("상태없음"),
    ATTENDANCE("출근"),
    EARLY_LEAVE_WORK("조퇴"),
    LEAVE_WORK("퇴근");

    private final String name;
}

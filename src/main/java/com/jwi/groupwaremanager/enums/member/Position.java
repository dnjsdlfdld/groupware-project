package com.jwi.groupwaremanager.enums.member;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Position {
    CEO("사장"),
    VICE_PRESIDENT("부사장"),
    DIRECTOR("부장"),
    CONDUCTOR("차장"),
    SECTION_CHIEF("과장"),
    DEPUTY("대리"),
    EMPLOYEE("사원");

    private final String name;
}

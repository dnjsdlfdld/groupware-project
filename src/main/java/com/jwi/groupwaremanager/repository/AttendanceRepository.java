package com.jwi.groupwaremanager.repository;

import com.jwi.groupwaremanager.entity.Attendance;
import org.springframework.data.jpa.repository.JpaRepository;


import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface AttendanceRepository extends JpaRepository<Attendance, Long> {
    // 특정 직원이 이 날에 데이터 있는지 없는지
    Optional<Attendance> findByDateWorkAndMemberId(LocalDate dateWork, long memberId);
    List<Attendance> findAllByDateWorkLessThanEqualAndDateWorkGreaterThanEqual(LocalDate dateStart, LocalDate dateEnd);

    long countByMember_IdAndDateWorkGreaterThanEqualAndDateWorkLessThanEqual(Long id, LocalDate dateStart, LocalDate dateEnd);

}

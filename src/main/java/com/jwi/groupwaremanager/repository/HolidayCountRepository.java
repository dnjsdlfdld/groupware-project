package com.jwi.groupwaremanager.repository;

import com.jwi.groupwaremanager.entity.HolidayCount;
import org.springframework.data.jpa.repository.JpaRepository;


public interface HolidayCountRepository extends JpaRepository<HolidayCount, Long> {
}

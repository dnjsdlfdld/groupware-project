package com.jwi.groupwaremanager.repository;

import com.jwi.groupwaremanager.entity.Banner;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BannerRepository extends JpaRepository<Banner, Long> {
    List<Banner> findTop5ByOrderByIdDesc();
}

package com.jwi.groupwaremanager.repository;

import com.jwi.groupwaremanager.entity.Member;
import com.jwi.groupwaremanager.enums.member.Position;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;


public interface MemberRepository extends JpaRepository<Member, Long> {
    List<Member> findAllByPositionAndIsEnableTrueOrderByDateInDesc(Position position);

    List<Member> findAllByDepartmentAndIsEnableTrueOrderByDateInDesc(String department);

    List<Member> findAllByNameAndIsEnableTrue(String name);

    Optional<Member> findByUsernameAndIsAdminTrue(String username);

    Optional<Member> findByUsernameAndIsAdminFalse(String username);

    long countByUsername(String username);

    Member findByUsername(String username);

    //List<Member> findAllByLoginTime(LocalDateTime loginTime);
}

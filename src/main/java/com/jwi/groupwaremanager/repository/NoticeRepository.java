package com.jwi.groupwaremanager.repository;

import com.jwi.groupwaremanager.entity.Member;
import com.jwi.groupwaremanager.entity.Notice;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface NoticeRepository extends JpaRepository<Notice, Long> {
    List<Notice> findTop5ByOrderByIdDesc();
}

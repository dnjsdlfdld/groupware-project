package com.jwi.groupwaremanager.repository;

import com.jwi.groupwaremanager.entity.HolidayHistory;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HolidayHistoryRepository extends JpaRepository<HolidayHistory, Long> {
}

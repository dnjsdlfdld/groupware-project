package com.jwi.groupwaremanager.repository;

import com.jwi.groupwaremanager.entity.HolidayUpload;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface HolidayUploadRepository extends JpaRepository<HolidayUpload, Long> {
    List<HolidayUpload> findAllByStartHolidayGreaterThanEqualAndStartHolidayLessThanEqualOrderByIdDesc
            (LocalDateTime dateStart, LocalDateTime dateEnd);

    Optional<HolidayUpload> findByIdAndMember_Id(long uploadId, long memberId);

    List<HolidayUpload> findAllByDateCreateGreaterThanEqualAndDateCreateLessThanEqualAndMember_Id
            (LocalDateTime dateStartTime, LocalDateTime dateEndTime, Long member_id);
}

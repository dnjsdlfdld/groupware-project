package com.jwi.groupwaremanager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GroupwareManagerApplication {

    public static void main(String[] args) {
        SpringApplication.run(GroupwareManagerApplication.class, args);
    }

}
